<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset="UTF-8">
   <title>
         @if(!$user == 0)
            {{ $user->name }}
         @else
                Tgs
         @endif
    </title>
   <link rel="icon" href="{{ asset('/') }}/sohan/images/title.png">

    <!-- ====== Google Fonts ====== -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600" rel="stylesheet">

    <!-- ====== ALL CSS ====== -->
    <link rel="stylesheet" href="{{ asset('/') }}/sohan/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}/sohan/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}/sohan/css/lightbox.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}/sohan/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}/sohan/css/animate.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="{{ asset('/') }}/sohan/css/responsive.css">
    <link rel="stylesheet" href="{{ asset('/') }}/sohan/css/custom.css">

</head>

<body data-spy="scroll" data-target=".navbar-nav">
    <a href="#" class="scrollup"><i class="far fa-hand-pointer fa-2x"></i></a> 
    <!-- ====== Header ====== -->
    <header id="header" class="header">
        <!-- ====== Navbar ====== -->
        <nav class="navbar navbar-expand-lg fixed-top">
            <div class="container">
                <!-- Logo -->
                <a class="navbar-brand logo" href="{{ url('/') }}">
                 @if(!$user == Null)
                   {{ $user->name }}
                 @else
                    
                 @endif     
                </a>
                <!-- // Logo -->

                <!-- Mobile Menu -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-expanded="false"><span><i class="fa fa-bars"></i></span></button>
                <!-- Mobile Menu -->

                <div class="collapse navbar-collapse main-menu" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item"><a class="nav-link" href="{{ url('/') }}">HOME</a></li>
                        <li class="nav-item"><a class="nav-link active" href="{{ route('blog') }}">BLOG</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- ====== // Navbar ====== -->

        <!-- Page Title -->
        <div class="page-title bg-img section-padding bg-overlay" style="background-image: url({{ asset('/') }}/sohan/images/breadcrumb.jpg)">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6 text-center">
                        <h2>Blog</h2>
                    </div>
                </div>
            </div>
        </div>
        <!-- // Page Title -->
    </header>
    <!-- ====== // Header ====== -->

    <section class="blog-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="blog-area">
                        <div class="row">
                           
@if(!isset($_GET['id']))

                    @foreach($blog as $value)
                            <div class="col-sm-6">
                                <div class="single-blog">
                                    <div class="blog-thumb" style="background-image: url({{ asset($value->image) }}"></div>
                                    <h4 class="blog-title"><a href="single-blog.html">{{ $value->website }}</a></h4>
                                    <p class="blog-meta"><a href="#">{{ $value->name }}</a>, {{ $value->date }}</p>
                                    <p>
                            {{ $value->short }}.
                                    </p>
                                    <a href="{{ route('singBlog',['id'=>$value->id]) }}" class="button">Read More</a>
                                </div>
                            </div>
                    @endforeach
@else
             @foreach($MyBlog as $value)
                            <div class="col-sm-6">
                                <div class="single-blog">
                                    <div class="blog-thumb" style="background-image: url({{ asset($value->image) }}"></div>
                                    <h4 class="blog-title"><a href="single-blog.html">{{ $value->website }}</a></h4>
                                    <p class="blog-meta"><a href="#">{{ $value->name }}</a>, {{ $value->date }}</p>
                                    <p>
                            {{ $value->short }}.
                                    </p>
                                    <a href="{{ route('singBlog',['id'=>$value->id]) }}" class="button">Read More</a>
                                </div>
                            </div>
                    @endforeach
       
@endif        
                    </div>
                        <div class="row justify-content-center">
                            <div class="col-lg-6 text-center">
                                <a href="#" class="button blog-btn">Load More</a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Sidebar -->
                <div class="col-lg-4">
                    <div class="blog-sidebar">
                        <!-- Sidebar Widget -->
                        <div class="sidebar-widget">
                            <h2 class="sidebar-title">Search here</h2>
                            <form action="blog.html" method="get">
                                <input type="search" placeholder="Search">
                                <button><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                        <!-- Sidebar Widget -->

                        <!-- Sidebar Widget -->
                        <div class="sidebar-widget">
                            <h2 class="sidebar-title">Categories</h2>
                            <ol>
                               @foreach($category as $value) 
                                <li><a href="{{ route('blog',['id'=>$value->id]) }}">{{ $value->category }}</a></li>
                               @endforeach
                            </ol>
                        </div>
                        <!-- Sidebar Widget -->
                <!--        
                        <div class="sidebar-widget">
                            <h2 class="sidebar-title">RECENT POSTS</h2>
                            <ol class="recent-post">
                              
                                <li>
                                    <span style="background-image: url({{ asset('/') }}/sohan/images/blog/img-1.jpg)"></span>
                                    <div class="blog-content">
                                        <a href="single-blog.html">Lorem ipsum dolor sit amet onsectetur</a>
                                        <div class="blog-date">22 FEB 2018</div>
                                    </div>
                                </li>

                            </ol>
                        </div>
                    -->                   
                        <div class="sidebar-widget">
                            <h2 class="sidebar-title">POPULAR TAGS</h2>
                            <ol class="tags">
                                <li><a href="#">Design</a></li>
                                <li><a href="#">Creative</a></li>
                                <li><a href="#">Idea</a></li>
                                <li><a href="#">Web Design</a></li>
                                <li><a href="#">HTML Template</a></li>
                                <li><a href="#">Wordpress</a></li>
                                <li><a href="#">Other</a></li>
                            </ol>
                        </div>
                       
                    </div>
                </div>
                <!-- // Sidebar -->
            </div>
        </div>
    </section>

    <!-- ====== Footer Area ====== -->
    <footer class="footer-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="copyright-text">
                         <p class="text-white">&copy; 2020 <a href="https://www.facebook.com/tgs.sohan" target="_blank">Made by Mohammad Sohan Tgs</a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ====== // Footer Area ====== -->

    <!-- ====== ALL JS ====== -->
    <script src="{{ asset('/') }}/sohan/js/jquery-3.3.1.min.js"></script>
    <script src="{{ asset('/') }}/sohan/js/bootstrap.min.js"></script>
    <script src="{{ asset('/') }}/sohan/js/lightbox.min.js"></script>
    <script src="{{ asset('/') }}/sohan/js/owl.carousel.min.js"></script>
    <script src="{{ asset('/') }}/sohan/js/jquery.mixitup.js"></script>
    <script src="{{ asset('/') }}/sohan/js/wow.min.js"></script>
    <script src="{{ asset('/') }}/sohan/js/typed.js"></script>
    <script src="{{ asset('/') }}/sohan/js/main.js"></script>
    <script src="{{ asset('/') }}/sohan/js/custom.js"></script>

</body>

</html>
