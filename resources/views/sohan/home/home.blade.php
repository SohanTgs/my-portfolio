<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset="UTF-8">
    <title>
         @if(!$user == 0)
            {{ $user->name }}
         @else
                Tgs
         @endif
    </title>
   <link rel="icon" href="{{ asset('/') }}/sohan/images/title.png">
    <!-- ====== Google Fonts ====== -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600" rel="stylesheet">
    <!-- ====== ALL CSS ====== -->
    <link rel="stylesheet" href="{{ asset('/') }}/sohan/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}/sohan/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}/sohan/css/lightbox.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}/sohan/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}/sohan/css/animate.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="{{ asset('/') }}/sohan/css/responsive.css">
    <link rel="stylesheet" href="{{ asset('/') }}/sohan/css/custom.css">
</head>
<body data-spy="scroll" data-target=".navbar-nav">
   <a href="#" class="scrollup"><i class="far fa-hand-pointer fa-2x"></i></a> 
     <!-- Preloader -->
    <div class="preloader">
        <div class="spinner">
            <div class="cube1"></div>
            <div class="cube2"></div>
        </div>
    </div>
    <!-- // Preloader -->
    
    <!-- ====== Header ====== -->
    <header id="header" class="header">
        <!-- ====== Navbar ====== -->
        <nav class="navbar navbar-expand-lg fixed-top">
            <div class="container">
                <!-- Logo -->
                <a class="navbar-brand logo" href="{{ url('/') }}">
                    @if(!$user == 0)
                    <img src="{{ asset($user->image) }}" alt="logo" style="border-radius: 60%;">
                    @else

                    @endif    
                </a>
                <!-- // Logo -->

                <!-- Mobile Menu -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-expanded="false"><span><i class="fa fa-bars"></i></span></button>
                <!-- Mobile Menu -->

                <div class="collapse navbar-collapse main-menu" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active"><a class="nav-link" href="#home">HOME</a></li>
                        <li class="nav-item"><a class="nav-link" href="#about">ABOUT</a></li>
                        <li class="nav-item"><a class="nav-link" href="#service">SERVICE</a></li>
                        <li class="nav-item"><a class="nav-link" href="#portfolio">GELLARY</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('blog') }}">BLOG</a></li>
                        <li class="nav-item"><a class="nav-link pr0" href="#contact">CONTACT</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- ====== // Navbar ====== -->
    </header>
    <!-- ====== // Header ====== -->

    <!-- ====== Hero Area ====== -->
    <div class="hero-aria" id="home">
        <!-- Hero Area Content -->
        <div class="container">
            <div class="hero-content h-100">
                <div class="d-table">
                    <div class="d-table-cell">   
                        @if(!$user == 0)
                        <h2 class="text-uppercase">{{ $user->name }}</h2>
                        @else

                        @endif    
                        <h3 class="text-uppercase"><span class="typed"></span></h3>
                        <p>
                        @if(!$user == 0)
                        {{ $user->email }}
                        @else

                        @endif 
                        </p>
                        <a href="#about" class="button smooth-scroll">Learn More</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- // Hero Area Content -->
        <!-- Hero Area Slider -->
        <div class="hero-area-slids owl-carousel">
            <div class="single-slider">
                <!-- Single Background -->
                @if(!$banner == 0)
                <div class="slider-bg" style="background-image: url({{ asset($banner->picture1) }})"></div>
                @else
                 <div class="slider-bg" style="background-image: url({{ asset('banner-image/b1.jpg') }})"></div>
                @endif    
                <!-- // Single Background -->
            </div>
            <div class="single-slider">
                <!-- Single Background -->
                @if(!$banner == 0)
                <div class="slider-bg" style="background-image: url({{ asset($banner->picture2) }})"></div> 
                @else
                  <div class="slider-bg" style="background-image: url({{ asset('banner-image/b2.jpg') }})"></div> 
                @endif   <!-- // Single Background -->
            </div>
        </div>
        <!-- // Hero Area Slider -->
    </div>
    <!-- ====== //Hero Area ====== -->

    <!-- ====== Featured Area ====== -->
    <section id="featured" class="section-padding pb-70">
        <div class="container">
            <div class="row">

                @foreach($education as $value)
                <div class="col-lg-3 col-md-6">
                    <div class="single-featured-item-wrap">
                        <h3 style="text-transform: uppercase;"><a href="#">{{ $value->degree }}</a></h3>
                        <div class="single-featured-item">
                            <div class="featured-icon">
                                <i class="fa fa-edit"></i>
                            </div>
                            <p><b>{{ $value->status }}</b></p>
                          <i>  <p style="text-transform: uppercase;">{{ $value->institute }}</p></i>
                            <p><b><i>{{ $value->result }}</i></b></p>
                          <!--  <p>CGPA <b>{{ $value->gpa }}</b> out of <b>{{ $value->of }}</b></p>  -->
                            <p><b>{{ $value->year}}</b></p>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </section>
    <!-- ====== //Featured Area ====== -->

    <!-- ====== About Area ====== -->
    <section id="about" class="section-padding about-area bg-light">
        <div class="container">
            <!-- Section Title -->
            <div class="row justify-content-center">
                <div class="col-lg-6 ">
                    <div class="section-title text-center">
                        <h2 id="pic">About Me</h2>
                        <p>
                        @if(!$user == 0)
                            {{ $user->short }}
                        @else 
                        
                        @endif    
                        </p>
                    </div>
                </div>
            </div>
            <!-- //Section Title -->
            <div class="row">
                <div class="col-lg-6">

<style type="text/css">
    #check{
        display: none;
        border:none;
    }
    #result{
        display: none;
    }
    #Dcv{
        margin-left: -339px;
    }
</style>

                 @if(!$user == 0)
              <div class="about-bg" style="background-image:url( {{ asset($user->image) }} )">
                  @else
                        
                  @endif          
                        <!-- Social Link -->
                        <div class="social-aria">
                            @foreach($link as $result)
                            <a target="_blank" href="{{ $result->link_name }}"><i class="fab fa-{{ $result->extention }}"></i></a>
                            @endforeach
                        </div>

     @foreach($cv as $value)  
  
    <form action="{{ route('SohanCV') }}" method="POST">
        @csrf
         <input type="hidden" name="id" value="{{ $value->id }}">
         <input type="submit" disabled="" name="btn" id="Dcv" value="Download Cv" class="btn btn-dark btn-sm">
         <input type="password" name="check" id="check">
                        <div id="result"></div>

     </form> <button class="btn btn-dark btn-sm" id='Dcv2' style="margin-top: -58px">Download Cv</button>                        
          @endforeach
                        <!-- // Social Link -->
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="about-content">
                        <h2>Hello, This is  <span>
                         @if(!$user == 0)
                            {{ $user->name }}
            
                        @else 
                        
                        @endif 
                        </span></h2>
                        <h4>
                         @if(!$user == 0)
                            {{ $user->title }}
                        @else 
                        
                        @endif 
                        </h4>
                        <h2><span>
                        @if(!$user == 0)
                            {{ $user->full_name }}
                        @else 
                        
                        @endif 
                        </span></h2>
                        <p>
                        @if(!$user == 0)
                            {{ $user->about }}
                        @else 
                        
                        @endif 
                        </p>

                        <h5>My Skills</h5>

                        <!-- Skill Area -->
                        <div id="skills" class="skill-area">

                            <!-- Single skill -->
                            <div class="single-skill">
                                @foreach($showSkills as $skill)
                                <div class="skillbar" data-percent="{{ $skill->percentage }}%">
                                    <div class="skillbar-title"><span>{{ $skill->skill }}</span></div>
                                    <div class="skillbar-bar"></div>
                                    <div class="skill-bar-percent">{{ $skill->percentage }}%</div>
                                </div>
                                @endforeach
                            </div>

                        </div>
                        <!-- //Skill Area -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ====== // About Area ====== -->

    <!-- ====== Fact Counter Section ====== -->
    <!-- ====================================================================
            NOTE: You need to change  'data-count="10"' and 'p' Eliments 
        ===================================================================== -->
    <section class="section-padding pb-70 bg-img fact-counter" id="counter" style="background-image: url({{ asset('/') }}/sohan/images/fan-fact-bg.jpg)">
        <div class="container">
            <div class="row">
              
              @foreach($asset as $value)
                <div class="col-lg-3 co col-md-6 l-md-6 text-center">
                    <div class="single-fun-fact">
                        <h2><span class="counter-value" data-count="{{ $value->number }}">0</span>+</h2>
                        <p>{{ $value->heading }}</p>
                    </div>
                </div>
              @endforeach
             
            </div>
        </div>
    </section>
    <!-- ====== //Fact Counter Section ====== -->

    <!-- ====== Service Section ====== -->
    <section id="service" class="section-padding pb-70 service-area bg-light">
        <div class="container">
            <!-- Section Title -->
            <div class="row justify-content-center">
                <div class="col-lg-6 ">
                    <div class="section-title text-center">
                        <h2>Service</h2>
                        <p>History of my services</p>
                    </div>
                </div>
            </div>
            <!-- //Section Title -->

            <div class="row">

               @foreach($service as $value)     
                <div class="col-lg-4 col-md-6">
                    <div class="single-service">
                        <div class="service-icon">
                            <i class="fa fa-code"></i>
                        </div>
                        <h2>{{ $value->title }}</h2>
                        <p><b><i>{{ $value->company }}</i></b></p>
                        <p><b><i>{{ $value->position }}</i></b></p>
                        <p><b><i>{{ $value->location }}</i></b></p>
                        <p><b><i>{{ $value->year }}</i></b></p>
                    </div>
                </div>
               @endforeach 
                
            </div>

        </div>
    </section>
    <!-- ====== //Service Section ====== -->

    <!-- ====== Why choose Me Section ====== -->
    <section id="" class="section-padding why-choose-us pb-70">
        <div class="container">
            <!-- Section Title -->
            <div class="row justify-content-center">
                <div class="col-lg-6 ">
                    <div class="section-title text-center">
                        <h2>Why choose Me</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                </div>
            </div>
            <!-- //Section Title -->
            <div class="row">
                <!-- Single Why choose me -->
               
                @foreach($reason as $value)
                <div class="col-md-6">
                    <div class="single-why-me why-me-left">
                        <div class="why-me-icon">
                            <div class="d-table">
                                <div class="d-table-cell">
                                    <i class="fa fa-check"></i>
                                </div>
                            </div>
                        </div>
                        <h4>{{ $value->heading }}</h4>
                        <p>
                           {{ $value->description }} 
                        </p>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </section>
    <!-- ====== //Why choose Me Section ====== -->

    <!-- ====== Portfolio Section ====== -->
    <section id="portfolio" class="section-padding pb-85 portfolio-area bg-light">
        <div class="container">
            <!-- Section Title -->
            <div class="row justify-content-center">
                <div class="col-lg-6 ">
                    <div class="section-title text-center">
                        <h2>Gellary</h2>
                        <p>This is my photos section</p>
                    </div>
                </div>
            </div>
            <!-- //Section Title -->
            <div class="row justify-content-center">
                <!-- Work List Menu-->
                <div class="col-lg-8">
                    <div class="work-list text-center">
                       <!-- <ul>
                            <li class="filter" class="active" data-filter="all">ALL</li>
                            <li class="filter" data-filter=".web">Web Design</li>
                            <li class="filter" data-filter=".graphic">Graphic</li>
                            <li class="filter" data-filter=".logo">Logo</li>
                            <li class="filter" data-filter=".wp">Wordpress</li>
                            <li class="filter" data-filter=".other">Other</li>
                        </ul> -->
                    </div>
                </div>
                <!-- // Work List Menu -->
            </div>
            <div class="row portfolio">
                <!-- Single Portfolio -->
         
         @if(count($photo))     
               @foreach($photo as $value)  
                <div class="col-lg-4 col-md-6 mix wp graphic">
                    <div class="single-portfolio" style="background-image: url({{ asset($value->photo) }})">
                        <div class="portfolio-icon text-center">
                            <a data-lightbox='lightbox' href="{{ asset($value->photo) }}"><i class="fas fa-expand-arrows-alt"></i></a>
                        </div>
                        <div class="portfolio-hover">
                            <h4>{{ $value->about }}</h4>
                        </div>
                    </div>
                </div>
              @endforeach
        @else     
              <div class="col-lg-4 col-md-6 mix wp graphic">
                    <div class="single-portfolio" style="background-image: url({{ asset('gallery/g2.jpg') }})">
                        <div class="portfolio-icon text-center">
                            <a data-lightbox='lightbox' href="{{ asset('gallery/g2.jpg') }}"><i class="fas fa-expand-arrows-alt"></i></a>
                        </div>
                        <div class="portfolio-hover">
                            <h4>Bangladesh Cricket</h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mix wp graphic">
                    <div class="single-portfolio" style="background-image: url({{ asset('gallery/g3.jpg') }})">
                        <div class="portfolio-icon text-center">
                            <a data-lightbox='lightbox' href="{{ asset('gallery/g3.jpg') }}"><i class="fas fa-expand-arrows-alt"></i></a>
                        </div>
                        <div class="portfolio-hover">
                            <h4>Presentation</h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mix wp graphic">
                    <div class="single-portfolio" style="background-image: url({{ asset('gallery/g1.jpg') }})">
                        <div class="portfolio-icon text-center">
                            <a data-lightbox='lightbox' href="{{ asset('gallery/g1.jpg') }}"><i class="fas fa-expand-arrows-alt"></i></a>
                        </div>
                        <div class="portfolio-hover">
                            <h4>Author</h4>
                        </div>
                    </div>
                </div>
            
        @endif        
            </div>
        </div>
    </section>
    <!-- ====== // Portfolio Section ====== -->

   <!-- 
    <section class="section-padding faq-area bg-secondary">
        <div class="container">
           
            <div class="row justify-content-center">
                <div class="col-lg-6 ">
                    <div class="section-title text-center faq-title">
                        <h2>Frequently asked questions</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                </div>
            </div>
           
            <div class="row">
                <div class="col-lg-5">
                    <div class="faq-bg bg-img" style="background-image: url({{ asset('/') }}/sohan/images/faq.jpeg)"></div>
                </div>
                <div class="col-lg-7">
                    
                    <div class="faq-content" id="accordion">
                      
                       <div class="single-faq">
          
                         <h4 class="collapsed" data-toggle="collapse" data-target="#faq-1">Collapsible Group Item One</h4>
                                                    
                            <div id="faq-1" class="collapse show" data-parent="#accordion">
                                <div class="faq-body">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat illo et, eum rerum nihil totam. Dolore atque quasi aspernatur fugiat commodi pariatur dignissimos, similique deleniti alias cumque, ea dolorum maiores, reprehenderit iusto quo officiis magni quibusdam est illum repellat adipisci quam qui error fugit? Fuga quam doloribus quas voluptas ducimus, adipisci minima quo consequatur ex!
                                </div>
                            </div>
                            
                        </div>
               
                        <div class="single-faq">
                       
                            <h4 class="collapsed" data-toggle="collapse" data-target="#faq-2">Lorem ipsum dolor sit amet Two</h4>
                                                     
                           <div id="faq-2" class="collapse" data-parent="#accordion">
                                <div class="faq-body">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat illo et, eum rerum nihil totam. Dolore atque quasi aspernatur fugiat commodi pariatur dignissimos, similique deleniti alias cumque, ea dolorum maiores, reprehenderit iusto quo officiis magni quibusdam est illum repellat adipisci quam qui error fugit? Fuga quam doloribus quas voluptas ducimus, adipisci minima quo consequatur ex!
                                </div>
                            </div>
              
                        </div>
                      
                        <div class="single-faq">                          
                          
                            <div id="faq-3" class="collapse" data-parent="#accordion">
                                <div class="faq-body">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat illo et, eum rerum nihil totam. Dolore atque quasi aspernatur fugiat commodi pariatur dignissimos, similique deleniti alias cumque, ea dolorum maiores, reprehenderit iusto quo officiis magni quibusdam est illum repellat adipisci quam qui error fugit? Fuga quam doloribus quas voluptas ducimus, adipisci minima quo consequatur ex!
                                </div>
                            </div>                  
                        </div>                    
                    </div>                  
                </div>
            </div>
        </div>
    </section>
    -->

    <!-- ====== Blog Section ====== -->
    <section class="section-padding pb-70 blog-section bg-light">
        <div class="container">
            <!-- Section Title -->
            <div class="row justify-content-center">
                <div class="col-lg-6 ">
                    <div class="section-title text-center">
                        <h2>Recent Work</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                </div>
            </div>
            <!-- //Section Title -->
            <div class="row">     

            @foreach($blog as $value)
                <div class="col-lg-4 col-md-6">
                    <div class="single-blog">
                        <div class="blog-thumb" style="background-image: url({{ asset($value->image) }})"></div>
                        <h4 class="blog-title"><a href="single-blog.html">{{ $value->website }}</a></h4>
                        <p class="blog-meta"><a href="#">{{ $value->name }}</a>, {{ $value->date }}</p>
                        <p>
                        {{ $value->short }}
                        .</p>
                        <a href="{{ route('singBlog',['id'=>$value->id]) }}" class="button">Read More</a>
                    </div>
                </div>
             @endforeach    

            </div>
        </div>
    </section>
    <!-- ====== // Blog Section ====== -->

    <!--
    <section id="testimonial" class="section-padding bg-secondary testimonial-area">
        <div class="container bg-white">
           
            <div class="row justify-content-center">
                <div class="col-lg-6 ">
                    <div class="section-title text-center">
                        <h2>Testimonial</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                </div>
            </div>
         
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="testimonials owl-carousel" data-ride="carousel">
                       
                        <div class="single-testimonial text-center">
                            <div class="testimonial-quote">
                                <i class="fa fa-quote-right"></i>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam nam suscipit similique quod eaque adipisci modi recusandae nesciunt veniam, ut, rem eligendi minima et, accusantium?</p>
                            <h4>Aseven M <span>CEO - aseven.info</span></h4>

                        </div>
                       
                        <div class="single-testimonial text-center">
                            <div class="testimonial-quote">
                                <i class="fa fa-quote-right"></i>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam nam suscipit similique quod eaque adipisci modi recusandae nesciunt veniam, ut, rem eligendi minima et, accusantium?</p>
                            <h4>Aseven M <span>CEO - aseven.info</span></h4>

                        </div>
         
                        <div class="single-testimonial text-center">
                            <div class="testimonial-quote">
                                <i class="fa fa-quote-right"></i>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam nam suscipit similique quod eaque adipisci modi recusandae nesciunt veniam, ut, rem eligendi minima et, accusantium?</p>
                            <h4>Aseven M <span>CEO - aseven.info</span></h4>

                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </section>
    -->

    <!-- ====== Team Section ====== -->
    <section class="section-padding pb-70 team-area">
        <div class="container">
            <!-- Section Title -->
            <div class="row justify-content-center">
                <div class="col-lg-6 ">
                    <div class="section-title text-center">
                        <h2>Familiers</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                </div>
            </div>

            <div class="row">
                
               @foreach($reference as $value)
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <h5 align="center">{{ $value->type }}</h5>
                    <div class="single-team">
                        <div class="team-thumb" style="background-image: url({{ asset($value->image) }})">
                            <div class="team-social">
                            @if(!$value->facebook == Null)   
                                <a target="_blank" href="https://www.facebook.com/{{ $value->facebook }}"><i class="fab fa-facebook-f"></i></a>
                            @else

                            @endif      

                            @if(!$value->twitter == Null)    
                                <a target="_blank" href="https://twitter.com/{{ $value->twitter }}"><i class="fab fa-twitter"></i></a>
                            @else
                                
                            @endif
                                    
                            </div>
                        </div>
                        <div class="team-content">
                            <h4>{{ $value->name }}</h4>
                            <span>{{ $value->profession }}</span>
                            <p>{{ $value->email }}</p>
                        </div>
                    </div>
                </div>
               @endforeach 

            </div>
        </div>
    </section>

    <!-- 
    <section class="section-padding call-to-action-aria bg-secondary">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <h2>Lorem ipsum dolor sit amet</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla fugit optio voluptatem modi, nemo, cupiditate vel, aspernatur, quae consequatur officia unde totam.</p>
                </div>
                <div class="col-lg-3">
                    <div class="cta-button">
                        <div class="d-table">
                            <div class="d-table-cell">
                                <a href="#" class="button">Contact me</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    -->

    <!-- 
    <section class="section-padding pb-70 pricing-area">
        <div class="container">
          
            <div class="row justify-content-center">
                <div class="col-lg-6 ">
                    <div class="section-title text-center">
                        <h2>Pricing Area</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                </div>
            </div>
           
            <div class="row">
              
                <div class="col-lg-3 col-md-6">
                    <div class="single-price-box text-center">
                        <div class="price-head">
                            <h2>Basic</h2>
                            <h3>$99<span>/ Per Month</span></h3>
                        </div>
                        <ul>
                            <li>Basic Feature</li>
                            <li>Best Output</li>
                            <li>Free Update</li>
                            <li>Live chat</li>
                        </ul>
                        <a href="#" class="button">PURCHASE NOW</a>
                    </div>
                </div>
 
                <div class="col-lg-3 col-md-6">
                    <div class="single-price-box text-center">
                        <div class="price-head">
                            <h2>Premium</h2>
                            <h3>$119<span>/ Per Month</span></h3>
                        </div>
                        <ul>
                            <li>Basic Feature</li>
                            <li>Best Output</li>
                            <li>Free Update</li>
                            <li>Live chat</li>
                        </ul>
                        <a href="#" class="button">PURCHASE NOW</a>
                    </div>
                </div>
      
                <div class="col-lg-3 col-md-6">
                    <div class="single-price-box text-center">
                        <div class="price-head">
                            <h2>Enterprise</h2>
                            <h3>$559<span>/ Per Month</span></h3>
                        </div>
                        <ul>
                            <li>Basic Feature</li>
                            <li>Best Output</li>
                            <li>Free Update</li>
                            <li>Live chat</li>
                        </ul>
                        <a href="#" class="button">PURCHASE NOW</a>
                    </div>
                </div>
  
                <div class="col-lg-3 col-md-6">
                    <div class="single-price-box text-center">
                        <div class="price-head">
                            <h2>Business</h2>
                            <h3>$999<span>/ Per Month</span></h3>
                        </div>
                        <ul>
                            <li>Basic Feature</li>
                            <li>Best Output</li>
                            <li>Free Update</li>
                            <li>Live chat</li>
                        </ul>
                        <a href="#" class="button">PURCHASE NOW</a>
                    </div>
                </div>
            
            </div>
        </div>
    </section>
   -->

    <!-- ====== Contact Area ====== -->
    <section id="contact" class="section-padding contact-section bg-light">
        <div class="container">
            <!-- Section Title -->
            <div class="row justify-content-center">
                <div class="col-lg-6 ">
                    <div class="section-title text-center">
                        <h2>Contact Me</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                </div>
            </div>
            <!-- //Section Title -->

            <!-- Contact Form -->
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <!-- Form -->

                    <form action="{{ route('contact') }}" method="POST" class="contact-form bg-white">
                       @csrf
                       <h4 align="center">{{ Session::get('message') }}</h4>
                        <div class="row">
                            <div class="col-lg-6 form-group">
                                <input type="text" class="form-control" name="name" required placeholder="Name">
                            </div>
                            <div class="col-lg-6 form-group">
                                <input type="email" class="form-control" name="email" required placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="subject" required placeholder="Subject">
                        </div>

                        <div class="form-group">
                            <textarea name="message" id="" class="form-control" required placeholder="Message"></textarea>
                        </div>
                        <div class="form-btn text-center">
                            <button class="button" type="submit">Send Message</button>
                            
                        </div>
                    </form>

                    <!-- // Form -->
                </div>
            </div>
            <!-- // Contact Form -->
        </div>
    </section>
    <!-- ====== // Contact Area ====== -->

    <!-- ====== Footer Area ====== -->
    <footer class="footer-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="copyright-text">
                        <p class="text-white">&copy; 2020 <a href="https://www.facebook.com/tgs.sohan" target="_blank">Made by Mohammad Sohan Tgs</a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ====== // Footer Area ====== -->

     <!-- ====== ALL JS ====== -->
   <script src="{{ asset('/') }}/sohan/js/jquery-3.3.1.min.js"></script>
    <script src="{{ asset('/') }}/sohan/js/bootstrap.min.js"></script>
    <script src="{{ asset('/') }}/sohan/js/lightbox.min.js"></script>
    <script src="{{ asset('/') }}/sohan/js/owl.carousel.min.js"></script>
    <script src="{{ asset('/') }}/sohan/js/jquery.mixitup.js"></script>
    <script src="{{ asset('/') }}/sohan/js/wow.min.js"></script>
    <script src="{{ asset('/') }}/sohan/js/typed.js"></script>
    <script src="{{ asset('/') }}/sohan/js/skill.bar.js"></script>
    <script src="{{ asset('/') }}/sohan/js/fact.counter.js"></script>
    <script src="{{ asset('/') }}/sohan/js/main.js"></script>
    <script src="{{ asset('/') }}/sohan/js/custom.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function(){
   
    $('#pic').dblclick(function(){
      $('#check').show();  

    });
 
        }); 
   </script>

<script>
document.getElementById('Dcv').disabled = true;

 var input = document.getElementById('check');
 input.oninput = function() {

    var xmlHttp = new XMLHttpRequest();
    var data = document.getElementById('check').value;
    var serverPage = 'http://localhost/imsohan00/public/ajax/'+data;
    xmlHttp.open('GET',serverPage);
    xmlHttp.onreadystatechange = function() {
          
      if(xmlHttp.readyState == 4 && xmlHttp.status == 200){
        document.getElementById('result').innerHTML = xmlHttp.responseText;
            if(xmlHttp.responseText == 'Have'){
                document.getElementById('Dcv').disabled = false;
                document.getElementById('Dcv').style.marginLeft = "0px";
                document.getElementById('Dcv2').style.display = "none";
                document.getElementById('check').style.display = "none"; 
            }else{
                document.getElementById('Dcv').disabled = true;
                document.getElementById('Dcv').style.marginLeft = "-339px";
                document.getElementById('Dcv2').style.display = "block"; 
                document.getElementById('Dcv2').style.marginTop = "-30px"; 
            }
      }
    }
    xmlHttp.send(null);
  }
</script>

</body>

</html>