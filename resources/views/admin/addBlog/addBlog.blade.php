@extends('admin.master')

@section('title')
	Add blog
@endsection('title')


@section('body')
	
	<div class="main-content">
	 
 
<br/><br/>

    <style type="text/css">
        .box{
            width: 650px;
            margin-left: 210px;
            margin-top: 30;
        }
    </style>  
    <h2 align="center">Add Blog</h2>
    <h3 align="center"><mark>
        {{ Session::get('message') }}

    </mark></h3>
    <a href="{{ route('manageBlog') }}" class="btn btn-primary">Go to list ?</a>
        <div class="box">
            <table class="table">
                <form action="{{ route('saveBlog') }}" method="POST" enctype= multipart/form-data>
                @csrf
                <tr>
                    <td><label for="category_id">Category*</label></td>  
                    <td>
                    	<select name="category_id" id="category_id">
                    		<option value="">--Select category name--</option>
                    		@foreach($category as $value)		
                    			<option value="{{ $value->id }}">{{ $value->category }}</option>
                    		@endforeach		
                    	</select>
                    	{{ $errors->has('category_id')?$errors->first('category_id'):'' }}
                    </td>    
                </tr>
                <tr>
                	<td><label for="website">Website Name*</label></td>
                	<td>
                		<input type="text" name="website" id="website" class="form-control">
                	{{ $errors->has('website')?$errors->first('website'):'' }}
                	</td>
                </tr>
                 <tr>
                    <td><label for="name">Made by (Name)*</label></td>  
                    <td><input type="text" id="name" name="name" class="form-control">
{{ $errors->has('name')?$errors->first('name'):'' }}
                    </td>    
                </tr>
                <tr>
                    <td><label for="date">Made by (Date)*</label></td>  
                    <td><input type="text" id="date" name="date" class="form-control">
{{ $errors->has('date')?$errors->first('date'):'' }}
                    </td>    
                </tr>
                 <tr>
                    <td><label for="short">Short description*</label></td>  
                    <td>
                    	<textarea id="short" maxlength="250" name="short" rows="2" cols="50"></textarea>
               {{ $errors->has('short')?$errors->first('short'):'' }}
                    </td>    
                </tr>
                  <tr>
                    <td><label for="long">Long description*</label></td>  
                    <td>
                    	<textarea id="long" maxlength="250" name="long" rows="2" cols="50"></textarea>
                     {{ $errors->has('long')?$errors->first('long'):'' }}
                    </td>    
                </tr>
                 <tr>
                    <td><label for="extra">Extra information</label></td>  
                    <td><input type="text" maxlength="250" id="extra" name="extra" class="form-control"></td>    
                </tr>
                <tr>
                    <td><label for="image">Image*</label></td>  
                    <td><input type="file" id="image" name="image" class="form-control">
 {{ $errors->has('image')?$errors->first('image'):'' }}
                    </td>    
                </tr>
                 <tr>
                    <td><label for="publication_status">Publication<br/>status*</label></td>  
                    <td>
                    Publish <input type="radio" value="1" checked="" id="publication_status" name="publication_status">|
                    Unpublish <input type="radio" value="0" name="publication_status" id="publication_status">
        {{ $errors->has('publication_status')?$errors->first('publication_status'):'' }} 
                    </td>    
                </tr>
                
                <tr>
                    <td></td>  
                    <td><input type="submit" value="Submit" class="btn btn-primary btn-lg"></td>    
                </tr> 
                </form>                 
            </table>
        </div>

        <br/>




	</div>

@endsection('body')