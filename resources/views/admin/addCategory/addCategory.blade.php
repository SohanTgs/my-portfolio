@extends('admin.master')

@section('title')
	Add category
@endsection('title')


@section('body')
	
	<div class="main-content">
	 
 
<br/><br/>

    <style type="text/css">
        .box{
            width: 650px;
            margin-left: 235px;
            margin-top: 40;
        }
    </style>  
    <h2 align="center">Add category</h2>
    <h3 align="center"><mark>
        {{ Session::get('message') }}

    </mark></h3>
        <div class="box">
            <table class="table">
                <form action="{{ route('saveCategory') }}" method="POST">
                @csrf
                <tr>
                    <td><label for="category">Add category*</label></td>  
                    <td><input type="text" id="category" name="category" class="form-control">
                    	{{ $errors->has('category')?$errors->first('category'):'' }}
                    </td>    
                </tr>
                 <tr>
                    <td><label for="description">Description</label></td>  
                    <td><input type="text" id="description" name="description" class="form-control"></td>    
                </tr>
                <tr>
                    <td></td>  
                    <td><input type="submit" value="Submit" class="btn btn-primary btn-lg"></td>    
                </tr> 
                </form>                 
            </table>
        </div>
     
        <br/>
<a href="{{ route('manageCategory') }}" class="btn btn-primary">Go to list ?</a>





	</div>

@endsection('body')