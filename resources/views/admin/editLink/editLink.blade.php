@extends('admin.master')

@section('title')
	Manage links 
@endsection('title')

@section('body')
		 <div class="main-content"><br/><br/>

    <style type="text/css">
    	.box{
    		width: 650px;
    		margin-left: 235px;
    		margin-top: 60px;
    	}

    </style> 
    
  

    <h2 align="center">Update Links</h2>
    <h3 align="center"><mark></mark></h3>
        <div class="box">
        	<table class="table">
        		<form action="{{ route('updateLink') }}" method="POST">
        		@csrf
        		<tr>
        			<td><h4><label for="link_name">Social website links</label></h5></td>

        			<td>
        				<input type="text" name="link_name" value="{{ $link->link_name }}" id="link_name" class="form-control">
        				
        			</td>
        		</tr>
        		<td><h4><label for="extention">Website extention</label></h5></td>
                        <input type="hidden" name="id" value="{{ $link->id }}">
        			<td>
        				<input type="text" name="extention" value="{{ $link->extention }}" id="extention" class="form-control">
        				
        			</td>
        		<tr>
        			<td><h4><label for="publication_status">Publication<br/> status</label></h5></td>
        			<td>
        			Publish <input type="radio"  name="publication_status" {{ $link->publication_status == 1 ? 'checked':'' }} value="1" id="publication_status"> |
        			Unpublish <input type="radio" {{ $link->publication_status == 0 ? 'checked':'' }} name="publication_status" value="0" id="publication_status">
        			
        			</td>
        		<tr>
        			<td></td>
        			<td><input type="submit" name="btn" value="Update links" class="btn btn-primary btn-lg"></td>
        		</tr>	
        		</form>         		
        	</table>
 </div>
 <a href="{{ route('links') }}" class="btn btn-primary">Add ?</a>
 <a href="{{ route('manageLinks') }}" class="btn btn-primary">Go to list ?</a>

@endsection('body')