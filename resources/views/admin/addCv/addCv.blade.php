@extends('admin.master')

   @section('title')
        Curriculum vita
   @endsection('title')


@section('body')
 
      <div class="main-content"><br/><br/>

    <style type="text/css">
    	.box{
    		width: 650px;
    		margin-left: 235px;
    		margin-top: 60px;
    	}
    </style>  
    <h2 align="center">Curriculum vita</h2>
    <h3 align="center"><mark>
    	{{ Session::get('message') }}
    	
    </mark></h3>
        <div class="box">
        	<table class="table">
        	<form action="{{ route('saveCv') }}" method="POST" enctype="multipart/form-data">
        		@csrf
        		<tr>
        			<td><h6><label for="file">Curriculum vita*</label></h6></td>
        			<td><input type="file" accept="*" id="file" name="file" class="form-control">
        				{{ $errors->has('file')?$errors->first('file'):'' }}
        			</td>
        		</tr>
				<tr>
        			<td><h6><label for="about">About</label></h6></td>
        			<td><input type="text" name="about" id="about" class="form-control">
        			{{ $errors->has('percentage')?$errors->first('percentage'):'' }}
        		</td>
        		</tr>
        		<tr>
        			<td><h6><label for="publication_status">Publication<br/> status*</label></h6></td>
        			<td>
        			Publish <input type="radio" name="publication_status" value="1" id="publication_status"> |
        			Unpublish <input type="radio" checked="" name="publication_status" value="0" id="publication_status">
        			{{ $errors->has('publication_status')?$errors->first('publication_status'):'' }}
        			</td>
        		<tr>
        			<td></td>
        			<td><input type="submit" name="btn" value="Submit" class="btn btn-primary btn-lg"></td>
        		</tr>	
        		</form>         		
        	</table>
        </div>	
<a href="{{ route('manageCv') }}" class="btn btn-primary">Go to list ?</a>
      </div>

@endsection('body')