@extends('admin.master')

@section('title')
	Manage Curriculum vita
@endsection('title')


@section('body')
	
	<div class="main-content">
<br/><br/>
		 <style type="text/css">
    	.box{
    		width: 990px;
    		margin-left: 30px;
    		margin-top: 5px;
    	}
    </style>  
    <h2 align="center">Curriculum vita</h2><a href="{{ route('addCv') }}" class="btn btn-primary">Add Cv ?</a>
    <h3 align="center"><mark>
        {{ Session::get('message') }}
    {{ Session::get('message2') }}
    </mark></h3>
        <div class="box">
        	<table class="table table-hover table-bordered">

        		<tr>
        			<th>SL No</th>
        			<th>Cv Name</th>
        			<th>Preview</th>
        			<th>About</th>
                    <th>Publication Status</th>
        			<th>Action</th>
                    
  				</tr>
  			@php($i=1)	
  		@foreach($cv as $value)	
  				<tr>
	  				<td>{{ $i++ }}</td>
	  				<td>{{ $value->file }}</td>
	  				<td>
	  					<iframe src="{{ url('storage/'.$value->file) }}" height=""></iframe>
	  				</td>
	  				@if(!$value->about == Null)
	  				<td>{{ $value->about }}</td>
	  				@else
	  					<td>Nothing</td>
	  				@endif	

	  				@if($value->publication_status == 1)
	  				<td>Published</td>
					@else
						<td>Unpublished</td>
	  				@endif	
  					<td>
                      	<a href="#"><i class="fa fa-edit fa-2x"></i></a> <br/>
                        <a href="{{ route('deleteCv',['id'=>$value->id]) }}"><i class="fa fa-trash"></i></a>
                         				<br/>
                       <a href="{{ route('cvView',['id'=>$value->id]) }}" class="fa fa-eye"></a><br/>
					<a href="{{ route('cvDownload',['file'=>$value->file]) }}" class="fa fa-download"></a> 
					<br/>

					@if($value->publication_status == 1)
						<a href="{{ route('unCv',['id'=>$value->id]) }}">Unpublished</a>
					@else
						<a href="{{ route('pubCv',['id'=>$value->id]) }}">Published</a>
					@endif		

                    </td>
                </tr>    
	       @endforeach	
        	</table>
        </div>
	  
	</div>

@endsection('body')