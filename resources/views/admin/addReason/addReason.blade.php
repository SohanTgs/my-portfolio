@extends('admin.master')

@section('title')
	Add reason
@endsection('title')


@section('body')
	
	<div class="main-content">


<br/><br/>
    <style type="text/css">
        .box{
            width: 650px;
            margin-left: 200px;
            margin-top: 40;
        }
    </style>  
    <h2 align="center">Add Reason</h2>
    <h3 align="center"><mark>
        {{ Session::get('message') }}

    </mark></h3>
        <div class="box">
            <table class="table">
                <form action="{{ route('saveReason') }}" method="POST">
                @csrf
                <tr>
                    <td><label for="heading">Reason heading*</label></td>  
                    <td><input type="text" id="heading" name="heading" class="form-control"></td>    
                </tr>
                 <tr>
                    <td><label for="description">Description*</label></td>  
                    <td>
                    	<textarea name="description" id="description" rows="2" cols="50"></textarea>
                    </td>    
                </tr>
                       
                <tr>
                    <td></td>  
                    <td><input type="submit" value="Submit" class="btn btn-primary btn-lg"></td>    
                </tr> 
                </form>                 
            </table>
        </div>
        <h6 align="center">
            {{ $errors->has('heading')?$errors->first('heading'):'' }}<br/>
             {{ $errors->has('description')?$errors->first('description'):'' }}<br/>

             
        </h6>
        <br/>
<a href="{{ route('manageReason') }}" class="btn btn-primary">Go to list ?</a>

</div>


	</div>

@endsection('body')