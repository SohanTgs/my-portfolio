@extends('admin.master')

   @section('title')
        Manage skills
   @endsection('title')


@section('body')
 
      <div class="main-content"><br/><br/>
      
         <style type="text/css">
    	.box{
    		width: 750px;
    		margin-left: 170px;
    		margin-top: 20px;
    	}
    </style>  
    <h2 align="center">Manage Skills</h2><a href="{{ route('addSkill') }}" class="btn btn-primary">Add Skill ?</a>
    <h3 align="center"><mark>{{ Session::get('message') }}{{ Session::get('message2') }}</mark></h3>
        <div class="box">
        	<table class="table table-hover table-bordered">

        		<tr>
        			<th>SL No</th>
        			<th>Name of skills</th>
        			<th>Actual performance of this topic</th>
        			<th>Publication status</th>
        			<th>Action</th>
        		</tr>
				@php($i=1)
				@foreach($showSkills as $skill)
				<tr>
        			<td>{{ $i++ }}</td>
        			<td>{{ $skill->skill }}</td>
        			<td>{{ $skill->percentage }}</td>
        			@if($skill->publication_status == 1)
        			<td>Published</td>
        			@else
        			<td>Unblished</td>
        			@endif
        			<td>
        				<a href="{{ route('editSkill',['id'=>$skill->id]) }}"><i class="fa fa-edit fa-2x"></i></a>
        				<a href="{{ route('deleteSkill',['id'=>$skill->id]) }}"><i class="fa fa-trash"></i></a>
        			</td>
        		</tr>
        	    @endforeach    		
        	</table>
        </div>

      </div>

@endsection('body')