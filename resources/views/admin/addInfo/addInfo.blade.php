@extends('admin.master')

@section('title')
	Experience & others
@endsection('title')

@section('body')

	<div class="main-content">
<br/><br/>		
		<div class="row">
			<div class="col-md-5"><br/>
				<h3 align="center">
					Add your experience or others
				</h3><br/>
				<form action="{{ route('saveInformation') }}" method="POST">
				@csrf
					<table width="95%">
						<tr>
							<td><label for="heading">Heading name:</label></td>
							<td><input type="text" name="heading" id="heading" class="form-control"></td>
						</tr>
						<tr>
							<td><label for="number">Yeras/Number/Clients/Others:</label></td>
							<td><input type="number" name="number" id="number" class="form-control"></td>
						</tr>
						<tr>
							<td><label for="status">Publication<br/>Status:</label></td>
							<td>
							Publish	<input type="radio" checked="" name="publication_status" id="status" value="1">|
							Unpublish	<input type="radio" name="publication_status" id="status" value="0">
							</td>
						</tr>
						<tr>
							<td></td>
							<td><input type="submit" name="btn" value="Submit" class="btn btn-primary"></td>
						</tr>
					</table>
				</form>

				<h6>{{ Session::get('message') }}</h6>
				{{ $errors->has('heading')?$errors->first('heading'):'' }}<br/>
				{{ $errors->has('number')?$errors->first('number'):'' }}<br/>
				{{ $errors->has('publication_status')?$errors->first('publication_status'):'' }}
			</div>
			<style type="text/css">
				#box{
					border-left: 3px solid black;
					height: 430px;
				}
			</style>
			<div class="col-md-7" id="box"> 
				<h3 align="center">
					Manage your informations
				</h3><br/>
				<h6>
					{{ Session::get('message2') }}
					{{ Session::get('message3') }}
				</h6>
				<table width="100%" align="center" class="table table-bordered table-hover">
					<tr>
						<th>SL No</th>
						<th>Headings</th>
						<th>Numbers</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
					@php($i=1)
					@foreach($asset as $result)
					<tr>
						<th>{{ $i++ }}</th>
						<th>{{ $result->heading }}</th>
						<th>{{ $result->number }}</th>
						@if($result->publication_status == 1)
						<th>Published</th>
						@else
							<th>Unpublished</th>
						@endif	
						<th>
							<a href="{{ route('editInfo',['id'=>$result->id]) }}"><i class="fa fa-edit fa-2x"></i></a>
							<a href="{{ route('deleteInfo',['id'=>$result->id]) }}"><i class="fa fa-trash"></i></a>
						</th>
					</tr>
				    @endforeach
				</table>
			</div>
		</div>

	</div>

@endsection('body')