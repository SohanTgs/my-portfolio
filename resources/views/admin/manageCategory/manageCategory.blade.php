@extends('admin.master')

@section('title')
	Manage category
@endsection('title')


@section('body')
	
	<div class="main-content">
	 <br/><br/>
      
         <style type="text/css">
    	.box{
    		width: 880px;
    		margin-left: 88px;
    		margin-top: 20px;
    	}
    </style>  
    <h2 align="center">Manage Category</h2><a href="{{ route('addCategory') }}" class="btn btn-primary">Add Category ?</a>
    <a href="{{ route('addBlog') }}" class="btn btn-primary">Add Blog ?</a>
    <h3 align="center"><mark>
        {{ Session::get('message') }}
        {{ Session::get('message2') }}
    </mark></h3>
        <div class="box">
        	<table class="table table-hover table-bordered">

        		<tr>
        			<th>SL No</th>
        			<th>Category</th>
                    <th>Description</th>
        			<th>Action</th>
                    
  				</tr>
  			@php($i=1)	
  			@foreach($category as $value)	
  				<tr>
	  				<td>{{ $i++ }}</td>
	  				<td>{{ $value->category }}</td>
	  				<td>{{ $value->description }}</td>
  					<td>
                      	<a href="#"><i class="fa fa-edit fa-2x"></i></a>
                        <a href="{{ route('deleteCategory',['id'=>$value->id]) }}"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>    
	       @endforeach	
        	</table>
        </div>


	</div>

@endsection('body')