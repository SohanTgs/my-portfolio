@extends('admin.master')

@section('title')
	Add service history
@endsection('title')

@section('body')
 
 <div class="main-content"><br/><br/>

    <style type="text/css">
        .box{
            width: 650px;
            margin-left: 235px;
            margin-top: 40;
        }
    </style>  
    <h2 align="center">Add Services history</h2>
    <h3 align="center"><mark>
        {{ Session::get('message') }}

    </mark></h3>
        <div class="box">
            <table class="table">
                <form action="{{ route('saveService') }}" method="POST">
                @csrf
                <tr>
                    <td><label for="title">Name of job titles*</label></td>  
                    <td><input type="text" id="title" name="title" class="form-control"></td>    
                </tr>
                 <tr>
                    <td><label for="company">Company*</label></td>  
                    <td><input type="text" id="company" name="company" class="form-control"></td>    
                </tr>
                <tr>
                    <td><label for="position">Position*</label></td>  
                    <td><input type="text" id="position" name="position" class="form-control"></td>    
                </tr>
                 <tr>
                    <td><label for="location">Location*</label></td>  
                    <td><input type="text" id="location" name="location" class="form-control"></td>    
                </tr>
                  <tr>
                    <td><label for="year">Year*</label></td>  
                    <td><input type="number" id="year" name="year" class="form-control"></td>    
                </tr>
                 <tr>
                    <td><label for="publication_status">Publication<br/>status*</label></td>  
                    <td>
                    Publish <input type="radio" value="1" checked="" id="publication_status" name="publication_status">|
                    Unpublish <input type="radio" value="0" name="publication_status" id="publication_status">
                    </td>    
                </tr>
                
                <tr>
                    <td></td>  
                    <td><input type="submit" value="Submit" class="btn btn-primary btn-lg"></td>    
                </tr> 
                </form>                 
            </table>
        </div>
        <h6 align="center">
            {{ $errors->has('title')?$errors->first('title'):'' }}<br/>
             {{ $errors->has('company')?$errors->first('company'):'' }}<br/>
              {{ $errors->has('position')?$errors->first('position'):'' }}<br/>
               {{ $errors->has('publication_status')?$errors->first('publication_status'):'' }}<br/>
                {{ $errors->has('year')?$errors->first('year'):'' }}<br/>
             
        </h6>
        <br/>
<a href="{{ route('manageService') }}" class="btn btn-primary">Go to list ?</a>
      </div>

@endsection('body')