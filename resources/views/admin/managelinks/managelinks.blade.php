@extends('admin.master')

@section('title')
	Manage Links
@endsection('title')

@section('body')
 
      <div class="main-content"><br/><br/>
      
         <style type="text/css">
    	.box{
    		width: 750px;
    		margin-left: 170px;
    		margin-top: 20px;
    	}
    </style>  
    <h2 align="center">Manage Links</h2><a href="{{ route('links') }}" class="btn btn-primary">Add links ?</a>
    <h3 align="center"><mark>{{ Session::get('message') }}{{ Session::get('message2') }}</mark></h3>
        <div class="box">
        	<table class="table table-hover table-bordered">

        		<tr>
        			<th>SL No</th>
        			<th>Name of website</th>
        			<th>Extention of website</th>
        			<th>Publication status</th>
        			<th>Action</th>
        		</tr>
        		@php($i=1)
        		@foreach($link as $result)
				<tr>
					<td>{{ $i++ }}</td>
					<td>{{ $result->link_name }}</td>
					<td>{{ $result->extention }}</td>
					@if($result->publication_status == 1)
					<td>Published</td>
					@else
					<td>Unpublished</td>
        			@endif
        			<td>
        				<a href="{{ route('editLink',['id'=>$result->id]) }}"><i class="fa fa-edit fa-2x"></i></a>
        				<a href="{{ route('deleteLink',['id'=>$result->id]) }}"><i class="fa fa-trash"></i></a>
        			</td>
        		</tr>
        	   @endforeach		
        	</table>
        </div>

      </div>

@endsection('body')