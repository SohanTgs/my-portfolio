@extends('admin.master')

@section('title')
	Manage blog
@endsection('title')


@section('body')
	
	<div class="main-content">
	 <br/><br/>
      
         <style type="text/css">
    	.box{
    		width: 960px;
    		margin-left: 60px;
    		margin-top: 20px;
    	}
    </style>  
    <h2 align="center">Manage Blogs</h2><a href="{{ route('addBlog') }}" class="btn btn-primary">Add Blog ?</a>
    <a href="{{ route('addCategory') }}" class="btn btn-primary">Add Category ?</a>
    <h3 align="center"><mark>
        {{ Session::get('message') }}
        {{ Session::get('message2') }}
        {{ Session::get('message3') }}
        {{ Session::get('message4') }}
    </mark></h3>
        <div class="box">
        	<table class="table table-hover table-bordered">

        		<tr>
        			<th>No</th>
        			<th>Website name</th>
                    <th>Date</th>
        			<th>Short description</th>
        			<th>Image</th>
     
        			<th>Action</th>
                    
  				</tr>
  			@php($i=1)	
  			@foreach($blog as $value)	
  				<tr>
	  				<td>{{ $i++ }}</td>
	  				<td>{{ $value->website }}</td>
	  				<td>{{ $value->date }}</td>
	  				<td>{{ $value->short }}</td>
	  				<td>
	  					<img src="{{ asset($value->image) }}" height="100" alt="Image">
	  				</td>

  					<td>
                      	<a href="#"><i class="fa fa-edit fa-2x"></i></a>
                        <a href="{{ route('deleteBlog',['id'=>$value->id]) }}"><i class="fa fa-trash"></i></a><br/>
                        @if($value->publication_status == 1)
                        	<a href="{{ route('unPub',['id'=>$value->id]) }}">Unpublished</a>
                        @else
                        	<a href="{{ route('Pub',['id'=>$value->id]) }}">Published</a>
                        @endif		
                    </td>
                </tr>    
	       @endforeach	
        	</table>
        </div>


	</div>

@endsection('body')