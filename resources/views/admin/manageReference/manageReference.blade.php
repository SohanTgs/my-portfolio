@extends('admin.master')

@section('title')
	Manage reference
@endsection('title')


@section('body')
	
	<div class="main-content">
	 <br/><br/>
      
         <style type="text/css">
    	.box{
    		width: 920px;
    		margin-left: 65px;
    		margin-top: 20px;
    	}
    </style>  
    <h2 align="center">Manage references</h2><a href="{{ route('addReference') }}" class="btn btn-primary">Add Reference ?</a>
    <h3 align="center"><mark>
        {{ Session::get('message') }}
        {{ Session::get('message2') }}
            {{ Session::get('message3') }}
        {{ Session::get('message4') }}
    </mark></h3>
        <div class="box">
        	<table class="table table-hover table-bordered">

        		<tr>
        			<th>SL No</th>
        			<th>Type</th>
        			<th>Name</th>
                    <th>Profession</th>
        			<th>Email</th>
        			<th>Status</th>
        			<th>Action</th>
                    
  				</tr>
  			@php($i=1)	
  			@foreach($reference as $value)	
  				<tr>
	  				<td>{{ $i++ }}</td>
	  				<td>{{ $value->type }}</td>
	  				<td>{{ $value->name }}</td>
	  				<td>{{ $value->profession }}</td>
	  				<td>{{ $value->email }}</td>
	  				@if($value->publication_status == 1)
	  				<td>Published</td>
	  				@else
	  					<td>Unpublished</td>
	  				@endif	
  					<td>
                      	<a href="#"><i class="fa fa-edit fa-2x"></i></a>
                        <a href="{{ route('deleteReference',['id'=>$value->id]) }}"><i class="fa fa-trash"></i></a>
                        @if($value->publication_status == 1)
                        <a href="{{ route('unpubRef',['id'=>$value->id]) }}">Unpublished</a>
                        @else
                        	<a href="{{ route('pubRef',['id'=>$value->id]) }}">Published</a>
                        @endif	
                    </td>
                </tr>    
	       @endforeach	
        	</table>
        </div>




	</div>

@endsection('body')