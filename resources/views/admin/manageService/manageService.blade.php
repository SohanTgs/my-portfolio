@extends('admin.master')

@section('title')
	Manage service history
@endsection('title')

@section('body')
 
<div class="main-content"><br/><br/>
      
         <style type="text/css">
    	.box{
    		width: 920px;
    		margin-left: 82px;
    		margin-top: 20px;
    	}
    </style>  
    <h2 align="center">Manage Services</h2><a href="{{ route('addService') }}" class="btn btn-primary">Add Service ?</a>
    <h3 align="center"><mark>
        {{ Session::get('message') }}
        {{ Session::get('message2') }}
    </mark></h3>
        <div class="box">
        	<table class="table table-hover table-bordered">

        		<tr>
        			<th>SL No</th>
        			<th>Name of titles</th>
                    <th>Company</th>
        			<th>Position</th>
        			<th>Location</th>
        			<th>Years</th>
        			<th>Action</th>
                    
  				</tr>
  			@php($i=1)	
  			@foreach($service as $value)	
  				<tr>
	  				<td>{{ $i++ }}</td>
	  				<td>{{ $value->title }}</td>
	  				<td>{{ $value->company }}</td>
	  				<td>{{ $value->position }}</td>
	  				<td>{{ $value->location }}</td>
	  				<td>{{ $value->year }}</td>
  					<td>
                      	<a href="#"><i class="fa fa-edit fa-2x"></i></a>
                        <a href="{{ route('deleteService',['id'=>$value->id]) }}"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>    
	       @endforeach	
        	</table>
        </div>

      </div>

@endsection('body')