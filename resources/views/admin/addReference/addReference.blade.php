@extends('admin.master')

@section('title')
	Add reference
@endsection('title')


@section('body')
	
	<div class="main-content">
	 

 
<br/><br/>

    <style type="text/css">
        .box{
            width: 650px;
            margin-left: 205px;
            margin-top: 40;
        }
    </style>  
    <h2 align="center">Add reference</h2>
    <h3 align="center"><mark>
        {{ Session::get('message') }}

    </mark></h3>
        <div class="box">
            <table class="table">
                <form action="{{ route('saveReference') }}" method="POST" enctype= multipart/form-data>
                @csrf
                <tr>
                    <td><label for="type">Relation type*</label></td>  
                    <td><input type="text" id="type" name="type" class="form-control"></td>    
                </tr>
                 <tr>
                    <td><label for="name">Name*</label></td>  
                    <td><input type="text" id="name" name="name" class="form-control"></td>    
                </tr>
                <tr>
                    <td><label for="profession">Profession*</label></td>  
                    <td><input type="text" id="profession" name="profession" class="form-control"></td>    
                </tr>
                 <tr>
                    <td><label for="email">Email*</label></td>  
                    <td><input type="email" id="email" name="email" class="form-control"></td>    
                </tr>
                  <tr>
                    <td><label for="facebook">Facebook username</label></td>  
                    <td><input type="text" id="facebook" name="facebook" class="form-control"></td>    
                </tr>
                <tr>
                    <td><label for="twitter">Twitter username</label></td>  
                    <td><input type="text" id="twitter" name="twitter" class="form-control"></td>    
                </tr>
                 <tr>
                    <td><label for="image">Image</label></td>  
                    <td><input type="file" id="image" accept="*" name="image" class="form-control"></td>    
                </tr> 
                 <tr>
                    <td><label for="publication_status">Publication<br/>status*</label></td>  
                    <td>
                    Publish <input type="radio" value="1" checked="" id="publication_status" name="publication_status">|
                    Unpublish <input type="radio" value="0" name="publication_status" id="publication_status">
                    </td>    
                </tr>
                
                <tr>
                    <td></td>  
                    <td><input type="submit" value="Submit" class="btn btn-primary btn-lg"></td>    
                </tr> 
                </form>                 
            </table>
        </div>
        <h6 align="center">
            {{ $errors->has('type')?$errors->first('type'):'' }}<br/>
             {{ $errors->has('name')?$errors->first('name'):'' }}<br/>
              {{ $errors->has('email')?$errors->first('email'):'' }}<br/>
            {{ $errors->has('profession')?$errors->first('profession'):'' }}<br/>
               {{ $errors->has('publication_status')?$errors->first('publication_status'):'' }}
        </h6>
        <br/>
<a href="{{ route('manageReference') }}" class="btn btn-primary">Go to list ?</a>




	</div>

@endsection('body')