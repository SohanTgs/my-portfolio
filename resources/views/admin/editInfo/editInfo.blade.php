@extends('admin.master')

@section('title')
	Experience & others
@endsection('title')

@section('body')

	<div class="main-content">
<br/><br/>		
		
			
				<h3 align="center">
					Update information
				</h3><br/>
				<br/>
				<form action="{{ route('updateInfo') }}" method="POST">
				@csrf
					<table width="70%" align="center">
						<tr>
							<input type="hidden" name="id" value="{{ $asset->id }}">
							<td><label for="heading">Heading name:</label></td>
							<td><input type="text" name="heading" value="{{ $asset->heading }}" id="heading" class="form-control"></td>
						</tr>
						<tr>
							<td><label for="number">Yeras/Number/Clients/Others:</label></td>
							<td><input type="number" value="{{ $asset->number }}" name="number" id="number" class="form-control"></td>
						</tr>
						<tr>
							<td><label for="status">Publication<br/>Status:</label></td>
							<td>
							Publish	<input type="radio" name="publication_status" id="status" value="1"  {{ $asset->publication_status == 1 ? 'checked' : '' }}>|
							Unpublish	<input type="radio" name="publication_status" id="status" value="0"  {{ $asset->publication_status == 0 ? 'checked' : '' }}>
							</td>
						</tr>
						<tr>
							<td></td>
							<td><input type="submit" name="btn" value="Submit" class="btn btn-primary"></td>
						</tr>
					</table>
				</form>
			
	

	</div>

@endsection('body')