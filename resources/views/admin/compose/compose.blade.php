@extends('admin.master')

@section('title')
	Compose
@endsection('title')


@section('body')
	
	<div class="main-content">
	 <h2 align="center">Compose</h2>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" >
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="form.css" >
        <script src="form.js"></script>
    </head>
    <body >
    	<style type="text/css">
    		.container{
    			margin-top: : -100px;
    		}
    		.body{
    			width: 5px;
    		}
    	</style>
    <a href="{{ route('sentMail') }}" class="btn btn-primary">Sent</a>
    <a href="{{ route('inbox') }}" class="btn btn-primary">Inbox</a>
        <div class="container"><h4 align="center">{{ Session::get('message') }}</h4>
            <!-- Form Started -->
            <div class="container form-top">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
                        <div class="panel panel-danger">
                            <div class="panel-body">
                               <form action="{{ route('sentEmail') }}" method="POST">
                                  @csrf
                                    <div class="form-group">
                                        <label for="email"><i class="fa fa-envelope" aria-hidden="true"></i> Email*</label>
                                        <input type="email" name="email" id="email" class="form-control" placeholder="To">
{{$errors->has('email')?$errors->first('email'):''}}</div>
									<div class="form-group">
                                        <label for="name"><i class="fa fa-address-book" aria-hidden="true"></i> Name*</label>
                                        <input type="text" name="name" id="name" class="form-control" placeholder="Name">
{{$errors->has('name')?$errors->first('name'):''}}</div>                                    
                                    <div class="form-group">
                                        <label for="subject"><i class="fa fa-window-maximize" aria-hidden="true"></i> Subject*</label>
                                        <input type="text" name="subject" id="subject" class="form-control" placeholder="Subject">
{{$errors->has('subject')?$errors->first('subject'):''}}</div>                                    
                                    <div class="form-group">
                                        <label for="message"><i class="fa fa-comment" aria-hidden="true"></i> Message*</label>
                                        <textarea rows="5" cols="" id="message" name="message" class="form-control" placeholder="Type Your Message"></textarea>
{{$errors->has('message')?$errors->first('message'):''}}</div>
                                    <div class="form-group">
                                        <button class="btn btn-primary">Send E-Mail</button>
                                    </div>
                                </form>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Form Ended -->
        </div>
    </body>

	</div>

@endsection('body')