@extends('admin.master')

@section('title')
	Edit banner
@endsection('title')

@section('body')
 
      <div class="main-content"><br/><br/>
      
         <style type="text/css">
    	.box{
    		width: 750px;
    		margin-left: 170px;
    		margin-top: 20px;
    	}
    </style>  
    <h2 align="center">Edit Banner</h2>
    <h3 align="center"><mark>  </mark></h3>
        <div class="box">
         <form action="{{ route('updateBanner') }}" method="POST" enctype="multipart/form-data">
        	<table class="table table-hover table-bordered">
        		@csrf
        		<tr>
        		<input type="hidden" name="id" value="{{ $banner->id }}">
        			<th>Picture One :</th>
        			<th>Picture Two :</th>
        	
        		</tr>
             		<tr>
        		
        			<th><img src="{{ asset($banner->picture1) }}" width="200" style="border-radius: 50%"></th>
        			<th><img src="{{ asset($banner->picture2) }}" width="200" style="border-radius: 50%"></th>
        	
        		</tr>
        		<tr>
        		
        			<th><input type="file" name="picture1" accept="*"></th>
        			<th><input type="file" name="picture2" accept="*" value="1"></th>
        	
        		</tr>
                 <tr>
        		
        			<th></th>
        			<th><input type="submit" name="btn" value="Edit Banner" class="btn btn-primary btn-lg"></th>
        	
        		</tr>		
				
        			
        	</table>
        </form>
        </div>

      </div>

@endsection('body')