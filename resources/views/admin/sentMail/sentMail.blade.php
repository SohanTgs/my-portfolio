@extends('admin.master')

@section('title')
	Sent Email
@endsection('title')


@section('body')
	
	<div class="main-content">
	 <br/><br/>
      
         <style type="text/css">
    	.box{
    		width: 985x;
    		margin-left: 65;
    		margin-top: 20px;
    	}
    </style>  
    <h2 align="center">Sent e-mail</h2><a href="{{ route('compose') }}" class="btn btn-primary">Composer</a>
    <a href="{{ route('inbox') }}" class="btn btn-primary">Inbox</a>
    <h3 align="center"><mark>
        {{ Session::get('message') }}
       
    </mark></h3>
        <div class="box">
        	<table class="table table-hover table-bordered">

        		<tr>
        			<th>SL No</th>
        			<th>Name</th>
                    <th>To</th>
        			<th>Subject</th>
        			<th>Message</th>
        			<th>Date</th>
        			<th>Action</th>
                    
  				</tr>
  			@php($i=1)	
  			@foreach($sent as $value)	
  				<tr>
	  				<td>{{ $i++ }}</td>
	  				<td>{{ $value->name }}</td>
	  				<td>{{ $value->email }}</td>
	  				<td>{{ $value->subject }}</td>
	  				<td>{{ $value->message }}</td>
	  				<td>{{ $value->created_at }}</td>
  					<td>
                      	<a href="#"><i class="fa fa-reply fa-2x"></i></a>
                        <a href="{{ route('deleteSentEmail',['id'=>$value->id]) }}"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>    
	       @endforeach	
        	</table>
        </div>


	</div>

@endsection('body')