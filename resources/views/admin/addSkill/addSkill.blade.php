@extends('admin.master')

   @section('title')
        Add Skill
   @endsection('title')


@section('body')
 
      <div class="main-content"><br/><br/>

    <style type="text/css">
    	.box{
    		width: 650px;
    		margin-left: 235px;
    		margin-top: 60px;
    	}
    </style>  
    <h2 align="center">Add Skills</h2>
    <h3 align="center"><mark>{{ Session::get('message') }}</mark></h3>
        <div class="box">
        	<table class="table">
        		<form action="{{ route('newSkill') }}" method="POST">
        		@csrf
        		<tr>
        			<td><h4><label for="skill">Topics of skill*</label></h5></td>
        			<td><input type="text" name="skill" id="skill" class="form-control">
        				{{ $errors->has('skill')?$errors->first('skill'):'' }}
        			</td>
        		</tr>
				<tr>
        			<td><h4><label for="percentage">Percentage*</label></h5></td>
        			<td><input type="number" name="percentage" max="100" min="10" id="percentage" class="form-control">
        			{{ $errors->has('percentage')?$errors->first('percentage'):'' }}
        		</td>
        		</tr>
        		<tr>
        			<td><h4><label for="publication_status">Publication<br/> status</label></h5></td>
        			<td>
        			Publish <input type="radio" checked="" name="publication_status" value="1" id="publication_status"> |
        			Unpublish <input type="radio" name="publication_status" value="0" id="publication_status">
        			{{ $errors->has('publication_status')?$errors->first('publication_status'):'' }}
        			</td>
        		<tr>
        			<td></td>
        			<td><input type="submit" name="btn" value="Add skill" class="btn btn-primary btn-lg"></td>
        		</tr>	
        		</form>         		
        	</table>
        </div>	
<a href="{{ route('manageSkill') }}" class="btn btn-primary">Go to list ?</a>
      </div>

@endsection('body')