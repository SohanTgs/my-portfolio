@extends('admin.master')

@section('title')
	Manage Education
@endsection('title')

@section('body')
 
      <div class="main-content"><br/><br/>
      
         <style type="text/css">
    	.box{
    		width: 920px;
    		margin-left: 90px;
    		margin-top: 20px;
    	}
    </style>  
    <h2 align="center">Manage Education</h2><a href="{{ route('addEducation') }}" class="btn btn-primary">Add Education ?</a>
    <h3 align="center"><mark>
        {{ Session::get('message') }}
        {{ Session::get('message2') }}
    </mark></h3>
        <div class="box">
        	<table class="table table-hover table-bordered">

        		<tr>
        			<th>SL No</th>
        			<th>Degrees</th>
                    <th>Status</th>
        			<th>Institutes</th>
        			<th>Years</th>
        			<th>GPA</th>
                    <th>Out of</th>
                    <th>Action</th>
        		</tr>
            @php($i=1)
            @foreach($education as $value)
				<tr>
                    <th>{{ $i++ }}</th>
                    <th>{{ $value->degree }}</th>
                    <th>{{ $value->status }}</th>
                    <th>{{ $value->institute }}</th>
                    <th>{{ $value->year }}</th>
                    <th>{{ $value->gpa }} </th>
                    <th>{{ $value->of }} </th>
                    <th>
                        <a href="#"><i class="fa fa-edit fa-2x"></i></a> 
                        <a href="{{ route('deleteEducation',['id'=>$value->id]) }}"><i class="fa fa-trash"></i></a>
                    </th>
                </tr>    
	        @endforeach
        	</table>
        </div>

      </div>

@endsection('body')