@extends('admin.master')

@section('title')
	Add photo
@endsection('title')


@section('body')
	
	<div class="main-content">
	 
 
<br/><br/>

    <style type="text/css">
        .box{
            width: 650px;
            margin-left: 205px;
            margin-top: 40;
        }
    </style>  
    <h2 align="center">Add photo</h2>
    <h3 align="center"><mark>
        {{ Session::get('message') }}

    </mark></h3>
        <div class="box">
            <table class="table">
                <form action="{{ route('savePhoto') }}" method="POST" enctype= multipart/form-data>
                @csrf
                <tr>
                    <td><label for="photo">Photo*</label></td>  
                    <td><input type="file" id="photo" accept="*" name="photo" class="form-control"></td>    
                </tr>
                  <tr>
                    <td><label for="about">About*</label></td>  
                    <td><input type="text" id="about" name="about" class="form-control"></td>    
                </tr>
                <tr>
                	<td><label for="publication_status">Publication<br/>status</label></td>
                	<td>
                		Published <input type="radio" checked="" value="1" id="publication_status" name="publication_status">|
                		Unpublised <input type="radio" value="0" id="publication_status" name="publication_status">
                	</td>
                </tr>
                <tr>
                    <td></td>  
                    <td><input type="submit" value="Submit" class="btn btn-primary btn-lg"></td>    
                </tr> 
                </form>                 
            </table>
        </div>
        <h6 align="center">
            {{ $errors->has('photo')?$errors->first('photo'):'' }}<br/>
             {{ $errors->has('publication_status')?$errors->first('publication_status'):'' }}<br/>
        </h6>
        <br/>
<a href="{{ route('managePhoto') }}" class="btn btn-primary">Go to list ?</a>



	</div>

@endsection('body')