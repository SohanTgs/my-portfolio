@extends('admin.master')

   @section('title')
        Add Education
   @endsection('title')


@section('body')
 
      <div class="main-content"><br/><br/>

    <style type="text/css">
    	.box{
    		width: 650px;
    		margin-left: 235px;
    		margin-top: 40;
    	}
    </style>  
    <h2 align="center">Add Education</h2>
    <h3 align="center"><mark>
        {{ Session::get('message') }}

    </mark></h3>
        <div class="box">
        	<table class="table">
        		<form action="{{ route('saveEducation') }}" method="POST">
        		@csrf
        		<tr>
                    <td><label for="degree">Name of degree*</label></td>  
                    <td><input type="text" id="degree" name="degree" class="form-control"></td>    
                </tr>
                 <tr>
                    <td><label for="status">Complete status*</label></td>  
                    <td><input type="text" id="status" name="status" class="form-control"></td>    
                </tr>
                <tr>
                    <td><label for="institute">Institute*</label></td>  
                    <td><input type="text" id="institute" name="institute" class="form-control"></td>    
                </tr>
                <tr>
                    <td><label for="year">Year*</label></td>  
                    <td><input type="number" id="year" name="year" class="form-control"></td>    
                </tr>
                 <tr>
                    <td><label for="result">Result status*</label></td>  
                    <td><input type="text" id="result" name="result" class="form-control"></td>    
                </tr>
                <tr>
                    <td><label for="gpa">Gpa*</label></td>  
                    <td>
                        <input type="text" id="gpa" name="gpa"><label for="of">Out of*</label> 
                        <input type="text" id="of" name="of">
                    </td>    
                </tr>  
                <tr>
                    <td></td>  
                    <td><input type="submit" value="Submit" class="btn btn-primary btn-lg"></td>    
                </tr> 
        		</form>         		
        	</table>
        </div>
        <h6 align="center">
            {{ $errors->has('degree')?$errors->first('degree'):'' }}<br/>
             {{ $errors->has('status')?$errors->first('status'):'' }}<br/>
              {{ $errors->has('institute')?$errors->first('institute'):'' }}<br/>
               {{ $errors->has('year')?$errors->first('year'):'' }}<br/>
                {{ $errors->has('result')?$errors->first('result'):'' }}<br/>
                 {{ $errors->has('gpa')?$errors->first('gpa'):'' }}<br/>
                  {{ $errors->has('of')?$errors->first('of'):'' }}
        </h6>
        <br/>
<a href="{{ route('manageEducation') }}" class="btn btn-primary">Go to list ?</a>
      </div>

@endsection('body')