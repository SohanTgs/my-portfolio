@extends('admin.master')

@section('title')
	Manage photos
@endsection('title')


@section('body')
	
	<div class="main-content">
	 
<br/><br/>
      
         <style type="text/css">
    	.box{
    		width: 930px;
    		margin-left: 70px;
    		margin-top: 20px;
    	}
    </style>  
    <h2 align="center">Manage Photos</h2><a href="{{ route('addPhoto') }}" class="btn btn-primary">Add photo ?</a>
    <h3 align="center"><mark>
        {{ Session::get('message') }}
        {{ Session::get('message2') }}
    	{{ Session::get('message3') }}
        {{ Session::get('message4') }}
    </mark></h3>
        <div class="box">
        	<table class="table table-hover table-bordered">

        		<tr>
        			<th>SL No</th>
        			<th>Photo</th>
        			<th>About</th>
                    <th>Publication status</th>
        			<th>Action</th>
                    
  				</tr>
  			@php($i=1)	
  			@foreach($photo as $value)	
  				<tr>
	  				<td>{{ $i++ }}</td>
	  				<td>
	  					<img src="{{ asset($value->photo) }}" height="150">
	  				</td>
	  				
	  				@if(!$value->about == Null)
	  				<td>{{ $value->about }}</td>
	  				@else
	  					<td>Nothing to say</td>
	  				@endif
	  					
	  				@if($value->publication_status == 1)
	  				<td>Published</td>
	  				@else
	  					<td>Unpublished</td>
	  				@endif	
  					<td>
                      	<a href="#"><i class="fa fa-edit fa-2x"></i></a>
                        <a href="{{ route('deletePhoto',['id'=>$value->id]) }}"><i class="fa fa-trash"></i></a>
<br/>
                    @if($value->publication_status == 1)    
                        <a href="{{ route('unpublishPhoto',['id'=>$value->id]) }}">Unpublished</a>
                    @else
                    	  <a href="{{ route('publishPhoto',['id'=>$value->id]) }}">Published</a>
                    @endif
                    	    
                    </td>
                </tr>    
	       @endforeach	
        	</table>
        </div>

	</div>

@endsection('body')