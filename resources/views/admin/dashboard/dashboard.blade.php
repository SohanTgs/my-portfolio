@extends('admin.master')

   @section('title')
        Dashboard
   @endsection('title')


@section('body')
 
       <div class="main-content"><br/><br/><style type="text/css">.message{margin-left: 280px;}</style>
        <section class="section">
          <div class="section-header" >
            <h1>Profile</h1>   <h1 class="message">
            {{ Session::get('message') }}{{ Session::get('sm') }}{{ Session::get('sm2') }}

          </h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
              <div class="breadcrumb-item">Profile</div>
            </div>
          </div>
          <div class="section-body">
            <h2 class="section-title">Hi, {{ Auth::user()->name }}!</h2>
            <p class="section-lead">
              Change information about yourself on this page.
            </p>
<h4 style="background:black;color: white; width: 460px;" align="center">
  {{ Session::get('sms') }} {{ Session::get('sms2') }}
</h4>
            <div class="row mt-sm-4">
              <div class="col-12 col-md-12 col-lg-5">
                <div class="card profile-widget">
                  <div class="profile-widget-header">

                 <form action="{{ route('saveImage') }}" method="POST" enctype="multipart/form-data">
                  @csrf
                    <img alt="image" src="{{ asset(Auth::user()->image) }}" class="rounded-circle profile-widget-picture">
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="file" name="image" accept="*">
                   <input type="hidden" name="id" value="{{ Auth::user()->id }}">
                    <div class="profile-widget-items">
                      <div class="profile-widget-item">
                        <div class="profile-widget-item-label">Posts</div>
                        <div class="profile-widget-item-value">187</div>
                      </div>
                      <div class="profile-widget-item">
                        <div class="profile-widget-item-label">Followers</div>
                        <div class="profile-widget-item-value">6,8K</div>
                      </div>
                      <div class="profile-widget-item">
                        <div class="profile-widget-item-label">Following</div>
                        <div class="profile-widget-item-value">2,1K</div>
                      </div>
                    </div>
                  </div>
                  <div class="profile-widget-description">

      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 <input type="submit" name="btn" value="Save" class="btn btn-dark"> &nbsp;&nbsp; 
                 {{ $errors->has('image')?$errors->first('image'):'' }}
                   </form>  

                    <div class="profile-widget-name"><br/>{{ Auth::user()->full_name }} <div class="text-muted d-inline font-weight-normal"><div class="slash"></div> {{ Auth::user()->title }}</div></div>
                  {{ Auth::user()->short }}
                  <br/> <br/>
                  {{ Auth::user()->about }}  

                    </b>.
                  </div>
                  <div class="card-footer text-center">
                    <div class="font-weight-bold mb-2">Follow {{ Auth::user()->name }} On</div>
                    <a href="#" class="btn btn-social-icon btn-facebook mr-1">
                      <i class="fab fa-facebook-f"></i>
                    </a>
                    <a href="#" class="btn btn-social-icon btn-twitter mr-1">
                      <i class="fab fa-twitter"></i>
                    </a>
                    <a href="#" class="btn btn-social-icon btn-github mr-1">
                      <i class="fab fa-github"></i>
                    </a>
                    <a href="#" class="btn btn-social-icon btn-instagram">
                      <i class="fab fa-instagram"></i>
                    </a>
                  </div>

{{ $errors->has('oldPassword')?$errors->first('oldPassword'):'' }}<br/>
{{ $errors->has('newPassword')?$errors->first('newPassword'):'' }}

                <form action="{{ route('changePassword') }}" method="POST">
                  @csrf
                  <div class="tab">
                    <input type="hidden" name="id" value="{{ Auth::user()->id }}">
                      <table align="center" width="90%">
                          <tr>
                            <th><label for='oldPassword'>Current Password</label></th>
                            <td>
                                <input type="password" id="oldPassword" name="oldPassword" class="form-control">
                            </td>
                            <tr>
                              <th><label for='newPassword'>New Password</label></th>
                              <td>
                                <input type="password" name="newPassword" id="newPassword" class="form-control">
                              </td>
                              <tr>
                                <td></td>
                                <td>
                                  <input type="submit" name="btn" value="Submit" class="btn btn-primary">
                                </td>
                              </tr>
                            </tr>
                          </tr>
                      </table>
                  </div>
                </form>    
              
                </div>
              </div>
              <div class="col-12 col-md-12 col-lg-7">
                <div class="card">



                  <form method="POST" class="needs-validation" novalidate="" action="{{ route('editProfile') }}">
                   @csrf
                   <input type="hidden" name="id" value="{{ Auth::user()->id }}">
                    <div class="card-header">
                      <h4>Edit Profile</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                          <div class="form-group col-md-6 col-12">
                            <label for="name">Name</label>
                            <input type="text" id="name" name='name' class="form-control" value="{{ Auth::user()->name }}" required="">
                            <div class="invalid-feedback">
                              Please fill in the first name
                            </div>
                          </div>
                          <div class="form-group col-md-6 col-12">
                            <label for="full_name">Full Name</label>
                            <input type="text" id="full_name" name='full_name' class="form-control" value="{{ Auth::user()->full_name }}" required="">
                            <div class="invalid-feedback">
                              Please fill in the last name
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-7 col-12">
                            <label for="email">Email</label>
                            <input type="email" id="email" name="email" class="form-control" value="{{ Auth::user()->email }}" required="">
                            <div class="invalid-feedback">
                              Please fill in the email
                            </div>
                          </div>
                          <div class="form-group col-md-5 col-12">
                            <label for="phone">Phone</label>
                            <input type="tel" id="phone" maxlength="20" name="phone" value="{{ Auth::user()->phone }}" class="form-control" value="">
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-12">
                            <label for="about">About me</label><br/>
                            <textarea id="about" cols="62" rows="4" name="about" maxlength="255">
                              {{ Auth::user()->about }}
                            </textarea>
                          </div>
                        </div>
                         <div class="row">
                          <div class="form-group col-12">
                            <label for="short">Short me(About)</label><br/>
                            <textarea id="short" cols="62" rows="2" name="short" maxlength="255">
                              {{ Auth::user()->short }}
                            </textarea>
                          </div>
                        </div>
                        <div class="form-group col-md-12 col-12">
                            <label for="title">Title</label>
                            <input type="text" id="title" value="{{ Auth::user()->title }}" name='title' class="form-control" required="">
                            <div class="invalid-feedback">
                              Please fill in the first name
                            </div>
                          </div>
                          <div class="row">
                           <div class="form-group col-12">
                            <label for="location">Location</label><br/>
                            <textarea id="location" cols="62" rows="2" name="location" maxlength="255">
                              {{ Auth::user()->location }}
                            </textarea>
                          </div>
                          </div>
                      
                    </div>
                    <div class="card-footer text-right">
                      <button class="btn btn-primary">Save Changes</button>
                    </div>
                  </form>


                </div>
              </div>
            </div>
          </div>
        </section>
      </div>

@endsection('body')
     