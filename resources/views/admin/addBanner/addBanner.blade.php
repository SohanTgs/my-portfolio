@extends('admin.master')

@section('title')
	Add banner
@endsection('title')

@section('body')
 
      <div class="main-content"><br/><br/>

    <style type="text/css">
    	.box{
    		width: 650px;
    		margin-left: 235px;
    		margin-top: 60px;
    	}
    </style>  
    <h2 align="center">Add banner pictures</h2>
    <h4 align="center"><mark> 
          {{ $errors->has('picture1')?$errors->first('picture1'):'' }} 
          {{ $errors->has('picture2')?$errors->first('picture2'):'' }}
     </mark></h4>
        <div class="box"> 
        	<table class="table">
        		<form action="{{ route('newBanner') }}" method="POST" enctype="multipart/form-data">
        		@csrf
      			<tr>
      				<th><label for="picture1">Piture One :</label></th>
      				<td><input type="file" id="picture1" name="picture1" accept="*"></td>
      			</tr>
      			<tr>
      				<th><label for="picture2">Piture One :</label></th>
      				<td><input type="file" id="picture2" name="picture2" accept="*"></td>
      			</tr>
      			<tr>
      				<th></th>
      				<td><input type="submit" name="btn" value="Add banner" class="btn btn-primary btn-lg"></td>
      			</tr>
        		</form>         		
        	</table>
        </div>	

      </div>

@endsection('body')