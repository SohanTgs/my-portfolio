@extends('admin.master')

@section('title')
	Links 
@endsection('title')

@section('body')
		 <div class="main-content"><br/><br/>

    <style type="text/css">
    	.box{
    		width: 650px;
    		margin-left: 235px;
    		margin-top: 60px;
    	}

    </style> 
    
   <button data-toggle="collapse" class="btn btn-dark" data-target="#demo">Instructions</button>
			<div id="demo" class="collapse">
			<ul type="1">	
				<li>Firstly cope your website link or username with website root or main path and copy social link input area . like <h4> https://www.facebook.com/your_user_name</h4></li>
				<li>Secondly add your website extention Like<h4>facebook</h4> </li>
			</ul>
			</div> 

    <h2 align="center">Add Links</h2>
    <h3 align="center"><mark>{{ Session::get('message') }}</mark></h3>
        <div class="box">
        	<table class="table">
        		<form action="{{ route('newLink') }}" method="POST">
        		@csrf
        		<tr>
        			<td><h4><label for="link_name">Social website links*</label></h5></td>

        			<td>Coppy and paste your link...example
        				<input type="text" placeholder="https://www.facebook.com/Your user-name" name="link_name" id="link_name" class="form-control">
        				
        			</td>
        		</tr>
        		<td><h4><label for="extention">Website extention*</label></h5></td>

        			<td>Example 
        				<input type="text" placeholder="facebook" name="extention" id="extention" class="form-control">
        				
        			</td>
        		<tr>
        			<td><h4><label for="publication_status">Publication<br/> status</label></h5></td>
        			<td>
        			Publish <input type="radio" checked="" name="publication_status" value="1" id="publication_status"> |
        			Unpublish <input type="radio" name="publication_status" value="0" id="publication_status">
        			
        			</td>
        		<tr>
        			<td></td>
        			<td><input type="submit" name="btn" value="Add links" class="btn btn-primary btn-lg"></td>
        		</tr>	
        		</form>         		
        	</table>
 </div>
 <a href="{{ route('manageLinks') }}" class="btn btn-primary">Go to list ?</a>

@endsection('body')