@extends('admin.master')

   @section('title')
        Edit skill
   @endsection('title')


@section('body')
 
      <div class="main-content"><br/><br/>
      <style type="text/css">
    	.box{
    		width: 650px;
    		margin-left: 235px;
    		margin-top: 60px;
    	}
    </style>  
    <h2 align="center">Edit Skills</h2>
    <h3 align="center"></mark></h3>
        <div class="box">
        	<table class="table">
        		<form action="{{ route('saveEditSkill') }}" method="POST">
        		@csrf
        		<tr>
        			<input type="hidden" name="id" value="{{ $skill->id }}">
        			<td><h4><label for="skill">Topics of skill</label></h5></td>
        			<td><input type="text" value="{{ $skill->skill }}" name="skill" id="skill" class="form-control">
        			</td>
        		</tr>
				<tr>
        			<td><h4><label for="percentage">Percentage</label></h5></td>
        			<td><input type="number" value="{{ $skill->percentage }}" max="100" min="10" name="percentage" id="percentage" class="form-control">
        		</td>
        		</tr>
        		<tr>
        			<td><h4><label for="publication_status">Publication<br/> status</label></h5></td>
        			<td>
        			Publish <input type="radio" name="publication_status" value="1" id="publication_status" {{ $skill->publication_status == 1 ? 'checked' : '' }}> |
        			Unpublish <input type="radio" name="publication_status" value="0" id="publication_status" {{ $skill->publication_status == 0 ? 'checked' : '' }}>
        			</td>
        		<tr>
        			<td></td>
        			<td><input type="submit" name="btn" value="Update" class="btn btn-primary btn-lg"></td>
        		</tr>	
        		</form>         		
        	</table>
        </div>	
<a href="{{ route('manageSkill') }}" class="btn btn-primary">Go to list ?</a>
<a href="{{ route('addSkill') }}" class="btn btn-primary">Add ?</a>
      </div>
        

      </div>

@endsection('body')
     