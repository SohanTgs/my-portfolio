@extends('admin.master')

@section('title')
	Manage banner
@endsection('title')

@section('body')
 
      <div class="main-content"><br/><br/>
      
         <style type="text/css">
    	.box{
    		width: 750px;
    		margin-left: 170px;
    		margin-top: 20px;
    	}
    </style>  
    <h2 align="center">Manage Banner</h2>
    <h3 align="center"><mark> 
        {{ Session::get('message1') }}{{ Session::get('message2') }}
        {{ Session::get('message3') }}{{ Session::get('message4') }}
        {{ Session::get('message5') }}
     </mark></h3>
        <div class="box">
         <form action="" method="POST" enctype="multipart/form-data">
        	<table class="table table-hover table-bordered">

        		<tr>
        			<th>SL No</th>
        			<th>Picture One :</th>
        			<th>Picture Two :</th>
        			<th>Action</th>
        		</tr>
     
				<tr>
					<td>1</td>
                    @if(!$banner == 0)
					<td><img src="{{ asset($banner->picture1) }}" width="200"></td>
                    @else
                        <td></td>
                    @endif

                    @if(!$banner == 0)
					<td><img src="{{ asset($banner->picture2) }}" width="200"></td>
                    @else
                        <td></td>
                    @endif
        			<td>
                     @if(!$banner == 0)   
        				<a href="{{ route('editBanner',['id'=>$banner->id]) }}"><i class="fa fa-edit fa-2x"></i></a>
                     @else 
                        <a href="#"><i class="fa fa-edit fa-2x"></i></a>
                     @endif  
                     
                     @if(!$banner == 0) 
        				<a href="{{ route('deleteBanner',['id'=>$banner->id]) }}"><i class="fa fa-trash"></i></a>
                     @else
                        <a href="#"><i class="fa fa-trash"></i></a>  
                     @endif   
        			</td>
        		</tr>
        			
        	</table>

        </form>
        </div>
        @if($banner == NULL)
          <a href="{{ route('addBanner') }}" class="btn btn-primary btn-lg">Add Banner</a>
        @else
          
        @endif    
      </div>

@endsection('body')