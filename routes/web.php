<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::gzet('/', function () {
    return view('welcome');
});*/

Route::group(['middleware' => ['CheckUser']], function () {


Route::get('/home/addSkill',[
	'uses'=>'Sohan@addSkill',
	'as'=>'addSkill'
]);

Route::POST('/home/addSkill/newSkill',[
	'uses'=>'Sohan@newSkill',
	'as'=>'newSkill'
]);

Route::get('/home/manageSkill',[
	'uses'=>'Sohan@manageSkill',
	'as'=>'manageSkill'
]);

Route::get('/home/manageSkill/editSkill',[
	'uses'=>'Sohan@editSkill',
	'as'=>'editSkill'
]);

Route::get('/home/manageSkill/deleteSkill/{id}',[
	'uses'=>'Sohan@deleteSkill',
	'as'=>'deleteSkill'
]);

Route::POST('/home/manageSkill/editSkill/saveEditSkill',[
	'uses'=>'Sohan@saveEditSkill',
	'as'=>'saveEditSkill'
]);

Route::POST('/home/editProfile',[
	'uses'=>'Sohan@editProfile',
	'as'=>'editProfile'
]);

Route::get('/home/links',[
	'uses'=>'Sohan@links',
	'as'=>'links'
]);

Route::POST('/home/links/newLink',[
	'uses'=>'Sohan@newLink',
	'as'=>'newLink'
]);


Route::get('/home/manageLinks',[
	'uses'=>'Sohan@manageLinks',
	'as'=>'manageLinks'
]);


Route::get('/home/manageLinks/editLink/{id}',[
	'uses'=>'Sohan@editLink',
	'as'=>'editLink'
]);


Route::get('/home/manageLinks/deleteLink/{id}',[
	'uses'=>'Sohan@deleteLink',
	'as'=>'deleteLink'
]);

Route::POST('/home/manageLinks/editLink/updateLink',[
	'uses'=>'Sohan@updateLink',
	'as'=>'updateLink'
]);

Route::POST('/home/saveImage',[
	'uses'=>'Sohan@saveImage',
	'as'=>'saveImage'
]);

Route::get('/home/addBanner',[
	'uses'=>'Sohan@addBanner',
	'as'=>'addBanner'
]);

Route::POST('/home/addBanner/newBanner',[
	'uses'=>'Sohan@newBanner',
	'as'=>'newBanner'
]);

Route::get('/home/manageBanner',[
	'uses'=>'Sohan@manageBanner',
	'as'=>'manageBanner'
]);

Route::get('/home/manageBanner/editBanner/{id}',[
	'uses'=>'Sohan@editBanner',
	'as'=>'editBanner'
]);

Route::POST('/home/manageBanner/editBanner/updateBanner',[
	'uses'=>'Sohan@updateBanner',
	'as'=>'updateBanner'
]);

Route::get('/home/manageBanner/deleteBanner/{id}',[
	'uses'=>'Sohan@deleteBanner',
	'as'=>'deleteBanner'
]);

Route::get('/home/addInfo',[
	'uses'=>'Sohan@addInfo',
	'as'=>'addInfo'
]);

Route::POST('/home/addInfo/saveInformation',[
	'uses'=>'Sohan@saveInformation',
	'as'=>'saveInformation'
]);

Route::get('/home/addInfo/editInfo/{id}',[
	'uses'=>'Sohan@editInfo',
	'as'=>'editInfo'
]);

Route::POST('/home/addInfo/editInfo/updateInfo',[
	'uses'=>'Sohan@updateInfo',
	'as'=>'updateInfo'
]);

Route::get('/home/addInfo/deleteInfo/{id}',[
	'uses'=>'Sohan@deleteInfo',
	'as'=>'deleteInfo'
]);

Route::get('/home/addEducation',[
	'uses'=>'Sohan@addEducation',
	'as'=>'addEducation'
]);

Route::POST('/home/addEducation/saveEducation',[
	'uses'=>'Sohan@saveEducation',
	'as'=>'saveEducation'
]);

Route::get('/home/manageEducation',[
	'uses'=>'Sohan@manageEducation',
	'as'=>'manageEducation'
]);

Route::get('/home/manageEducation/deleteEducation/{id}',[
	'uses'=>'Sohan@deleteEducation',
	'as'=>'deleteEducation'
]);

Route::get('home/addService',[
	'uses'=>'Sohan@addService',
	'as'=>'addService'
]);

Route::get('home/manageService',[
	'uses'=>'Sohan@manageService',
	'as'=>'manageService'
]);

Route::POST('home/addService/saveService',[
	'uses'=>'Sohan@saveService',
	'as'=>'saveService'
]);

Route::get('home/manageService/deleteService/{id}',[
	'uses'=>'Sohan@deleteService',
	'as'=>'deleteService'
]);


Route::get('home/addReason',[
	'uses'=>'Sohan@addReason',
	'as'=>'addReason'
]);

Route::get('home/manageReason',[
	'uses'=>'Sohan@manageReason',
	'as'=>'manageReason'
]);

Route::POST('home/addReason/saveReason',[
	'uses'=>'Sohan@saveReason',
	'as'=>'saveReason'
]);

Route::get('home/manageReason/deleteReason/{id}',[
	'uses'=>'Sohan@deleteReason',
	'as'=>'deleteReason'
]);

Route::get('home/addPhoto',[
	'uses'=>'Sohan@addPhoto',
	'as'=>'addPhoto'
]);


Route::POST('home/addPhoto/savePhoto',[
	'uses'=>'Sohan@savePhoto',
	'as'=>'savePhoto'
]);

Route::get('home/managePhoto',[
	'uses'=>'Sohan@managePhoto',
	'as'=>'managePhoto'
]);

Route::get('home/managePhoto/deletePhoto/{id}',[
	'uses'=>'Sohan@deletePhoto',
	'as'=>'deletePhoto'
]);

Route::get('home/managePhoto/unpublishPhoto/{id}',[
	'uses'=>'Sohan@unpublishPhoto',
	'as'=>'unpublishPhoto'
]);

Route::get('home/managePhoto/publishPhoto/{id}',[
	'uses'=>'Sohan@publishPhoto',
	'as'=>'publishPhoto'
]);

Route::get('home/addReference',[
	'uses'=>'Sohan@addReference',
	'as'=>'addReference'
]);

Route::get('home/manageReference',[
	'uses'=>'Sohan@manageReference',
	'as'=>'manageReference'
]);

Route::POST('home/addReference/saveReference',[
	'uses'=>'Sohan@saveReference',
	'as'=>'saveReference'
]);

Route::get('home/manageReference/deleteReference/{id}',[
	'uses'=>'Sohan@deleteReference',
	'as'=>'deleteReference'
]);

Route::get('home/manageReference/pubRef/{id}',[
	'uses'=>'Sohan@pubRef',
	'as'=>'pubRef'
]);

Route::get('home/manageReference/unpubRef/{id}',[
	'uses'=>'Sohan@unpubRef',
	'as'=>'unpubRef'
]);

Route::get('home/inbox',[
	'uses'=>'SentEmail@inbox',
	'as'=>'inbox'
]);

Route::get('home/sentMail',[
	'uses'=>'SentEmail@sentMail',
	'as'=>'sentMail'
]);

Route::get('home/compose',[
	'uses'=>'SentEmail@compose',
	'as'=>'compose'
]);


Route::get('home/inbox/deleteContact/{id}',[
	'uses'=>'SentEmail@deleteContact',
	'as'=>'deleteContact'
]);

Route::POST('home/compose/sentEmail',[
	'uses'=>'SentEmail@sentEmail',
	'as'=>'sentEmail'
]);

Route::get('home/sentMail/deleteSentEmail/{id}',[
	'uses'=>'SentEmail@deleteSentEmail',
	'as'=>'deleteSentEmail'
]);

Route::POST('home/changePassword',[
	'uses'=>'Sohan@changePassword',
	'as'=>'changePassword'
]);

Route::get('home/addCategory',[
	'uses'=>'Blog@addCategory',
	'as'=>'addCategory'
]);

Route::POST('home/addCategory/saveCategory',[
	'uses'=>'Blog@saveCategory',
	'as'=>'saveCategory'
]);

Route::get('home/manageCategory',[
	'uses'=>'Blog@manageCategory',
	'as'=>'manageCategory'
]);

Route::get('home/manageCategory/deleteCategory/{id}',[
	'uses'=>'Blog@deleteCategory',
	'as'=>'deleteCategory'
]);

Route::get('home/addBlog',[
	'uses'=>'Blog@addBlog',
	'as'=>'addBlog'
]);

Route::POST('home/addBlog/saveBlog',[
	'uses'=>'Blog@saveBlog',
	'as'=>'saveBlog'
]);

Route::get('home/manageBlog',[
	'uses'=>'Blog@manageBlog',
	'as'=>'manageBlog'
]);

Route::get('home/manageBlog/deleteBlog/{id}',[
	'uses'=>'Blog@deleteBlog',
	'as'=>'deleteBlog'
]);

Route::get('home/unPub/{id}',[
	'uses'=>'Blog@unPub',
	'as'=>'unPub'
]);

Route::get('home/Pub/{id}',[
	'uses'=>'Blog@Pub',
	'as'=>'Pub'
]);

Route::get('home/addCv',[
	'uses'=>'Sohan@addCv',
	'as'=>'addCv'
]);

Route::get('home/manageCv',[
	'uses'=>'Sohan@manageCv',
	'as'=>'manageCv'
]);

Route::POST('home/addCv/saveCv',[
	'uses'=>'Sohan@saveCv',
	'as'=>'saveCv'
]);

Route::get('home/manageCv/cvView/{id}',[
	'uses'=>'Sohan@cvView',
	'as'=>'cvView'
]);

Route::get('home/manageCv/cvDownload/{file}',[
	'uses'=>'Sohan@cvDownload',
	'as'=>'cvDownload'
]);

Route::get('home/manageCv/deleteCv/{id}',[
	'uses'=>'Sohan@deleteCv',
	'as'=>'deleteCv'
]);

Route::get('home/manageCv/pubCv/{id}',[
	'uses'=>'Sohan@pubCv',
	'as'=>'pubCv'
]);

Route::get('home/manageCv/unCv/{id}',[
	'uses'=>'Sohan@unCv',
	'as'=>'unCv'
]);







}); // middleware end syntax






Route::get('/',[
	'uses'=>'Sohan@index',
	'as'=>'index'
]);

Route::get('/blog',[
	'uses'=>'Sohan@blog',
	'as'=>'blog'
]);

Route::get('/singBlog',[
	'uses'=>'Sohan@singBlog',
	'as'=>'singBlog'
]);

Route::POST('/contact',[
	'uses'=>'SentEmail@contact',
	'as'=>'contact'
]);

Route::get('/SohanCV/{id}',[
	'uses'=>'Sohan@SohanCV',
	'as'=>'SohanCV'
]);

Route::POST('/SohanCV',[
	'uses'=>'Sohan@SohanCV',
	'as'=>'SohanCV'
]);

Route::get('/ajax/{data}',[
	'uses'=>'Ajax@ajax',
	'as'  =>'ajax'
]);










Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
