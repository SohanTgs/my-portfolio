<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Skills;
use App\User;
use App\Link;
use App\Banner;
use App\Asset;
use App\Education;
use App\Service;
use App\Reason;
use App\Photo;
use App\Reference;
use App\MyBlog;
use App\Category;
use PDF;
use App\Cv;


class Sohan extends Controller
{
 	   public function index(){
    	$user = User::take(1)->first();
    	$showSkills = Skills::where('publication_status',1)->get();
    	$link = Link::where('publication_status',1)->get();
    	$banner = Banner::take(1)->first();
    	$asset = Asset::where('publication_status',1)->get();
    	$education = Education::orderBy('id', 'DESC')->get();
    	$service = Service::where('publication_status',1)->get();
    	$reason = Reason::all();
    	$photo = Photo::where('publication_status',1)->get();
    	$reference = Reference::where('publication_status',1)->get();
    	$blog = MyBlog::where('publication_status',1)->take(2)->get();
    	$cv = Cv::where('publication_status',1)->get();

    	return view('sohan.home.home',[
    		'showSkills'=>$showSkills,
    		'user'=>$user,
    		'link'=>$link,
    		'banner'=>$banner,
    		'asset'=>$asset,
    		'education'=>$education,
    		'service'=>$service,
    		'reason'=>$reason,
    		'photo'=>$photo,
    		'reference'=>$reference,
    		'blog'=>$blog,
    		'cv'=>$cv
    ]);
   
    
    }



   		 public function blog(){
   		 	$user = User::take(1)->first();
   		 	$category = category::all();
   		 	$blog = MyBlog::where('publication_status',1)->get();
   		 
   		 	if(isset($_GET['id'])){
   		 		$id = $_GET['id'];
   		 		$MyBlog = MyBlog::where('category_id',$id)->get();
   		 		return view('sohan.blog.blog',['MyBlog'=>$MyBlog,'blog'=>$blog,'category'=>$category,'user'=>$user]);
   		 	}else{
   		 		return view('sohan.blog.blog',['category'=>$category,'blog'=>$blog,'user'=>$user]);
   		 	}

    		

    }

   		 public function singBlog(){
   		 	$category = Category::all();
   		 	$user = User::take(1)->first();
   		 	if(isset($_GET['id'])){
   		 		$id = $_GET['id'];
   		 		$value = MyBlog::find($id);
   		 		return view('sohan.singBlog.singBlog',['value'=>$value,'user'=>$user,'category'=>$category]);
   		 	}else{
   		 		return view('sohan.singBlog.singBlog',['user'=>$user,'category'=>$category]);
   		 	}

    	
    }





   		 public function addSkill(){
    	return view('admin.addSkill.addSkill');
    }

   		 public function newSkill(Request $request){
		  $this->validate($request,[
		      'skill'=>'required|unique:skills',
		      'percentage'=>'required',
		      'publication_status'=>'required'
		    ]);
		  $skill = new Skills();
		  $skill->skill = $request->skill;
		  $skill->percentage = $request->percentage;
		  $skill->publication_status = $request->publication_status;
		  $skill->save();

		  return redirect('home/addSkill/')->with('message','Added skill successfully');
    }

   		 public function manageSkill(){
    	$showSkills = Skills::all();
    	return view('admin.manageSkill.manageSkill',['showSkills'=>$showSkills]);
    }

	 	public function editSkill(Request $request){
	 		$skill = Skills::find($request->id);
	    	return view('admin.editSkill.editSkill',['skill'=>$skill]);
	    }


	     public function saveEditSkill(Request $request){
	    	$skill = Skills::find($request->id);
	    	$skill->skill = $request->skill;
	    	$skill->percentage = $request->percentage;
	    	$skill->publication_status = $request->publication_status;
	    	$skill->save();
	    	return redirect('home/manageSkill')->with('message','Skill edited successfully');
	    }  

	 	public function deleteSkill($id){
	    	$skill = Skills::find($id);
	    	$skill->delete();
	    	return redirect('home/manageSkill')->with('message2','Skill deleted successfully');
	    }

	    public function editProfile(Request $request){
	    	$user = User::find($request->id);
	    	$user->name = $request->name;
	    	$user->email = $request->email;
	    	$user->full_name = $request->full_name;
	    	$user->about = $request->about;
	    	$user->short = $request->short;
	    	$user->title = $request->title;
	    	$user->phone = $request->phone;
	    	$user->location = $request->location;
	    	$user->save();
	    	return redirect('/home')->with('message','Profile updated successfully');
	    }

	    public function links(){
	    	return view('admin.links.links');
	    }

	    public function newLink(Request $request){
	    	$this->validate($request,[
	    		'link_name'=>'required:unique:links',
	    		'extention'=>'required',
	    		'publication_status'=>'required',
	    	]);
	    	$link = new link();
	    	$link->link_name = $request->link_name;
	    	$link->extention = $request->extention;
	    	$link->publication_status = $request->publication_status;
	    	$link->save();
	    	return redirect('home/links')->with('message','Link added successfully');
	    }  

 	    public function manageLinks(){
 		$link = link::all();
	    	return view('admin.managelinks.managelinks',['link'=>$link]);
	    }

	    public function editLink($id){
	    	$link = link::find($id);
	    	return view('admin.editLink.editLink',['link'=>$link]);
	    }
	   

	   	 public function updateLink(Request $request){
	    	$link = link::find($request->id);
	    	$link->link_name = $request->link_name;
	    	$link->extention = $request->extention;
	    	$link->publication_status = $request->publication_status;
	    	$link->save();
	    	return redirect('home/manageLinks')->with('message','Link eduted successfully');
	    }

	    public function deleteLink($id){
	    	$link = link::find($id);
	    	$link->delete();
	    	return redirect('home/manageLinks')->with('message2','Link deleted successfully');
		}

		public function saveImage(Request $request){
			$this->validate($request,[
				'image'=>'required'
			]);

			$user = User::find($request->id);
			if($user->image == Null){
				$Image = $request->file('image');
			    $Name = $Image->getClientOriginalName();
			    $directory = 'admin-image/';
			    $ImageUrl = $directory.$Name;
			    $Image->move($directory,$Name);
			    $user->image = $ImageUrl;
			    $user->save();
			    return redirect('home')->with('sms','Picture added successfully');
			}else{
				unlink($user->image);
				$Image = $request->file('image');
			    $Name = $Image->getClientOriginalName();
			    $directory = 'admin-image/';
			    $ImageUrl = $directory.$Name;
			    $Image->move($directory,$Name);
			    $user->image = $ImageUrl;
			    $user->save();
			    return redirect('home')->with('sms2','Picture updated successfully');
			}
			
}
			
		public function addBanner(){
			$banner = Banner::count();
			if($banner == 1){
				return view('admin.BannerCheck.BannerCheck');
			}
			else{
				return view('admin.addBanner.addBanner');
			}
			
		}

		public function newBanner(Request $request){	
			$this->validate($request,[
				'picture1'=>'required',
				'picture2'=>'required'
			]);

				$picture1 = $request->file('picture1');
			    $Name = $picture1->getClientOriginalName();
			    $directory = 'banner-image/';
			    $ImageUrl1 = $directory.$Name;
			    $picture1->move($directory,$Name);

			    $picture2 = $request->file('picture2');
			    $Name = $picture2->getClientOriginalName();
			    $directory = 'banner-image/';
			    $ImageUrl2 = $directory.$Name;
			    $picture2->move($directory,$Name);

			    $banner = new Banner();
			    $banner->picture1 = $ImageUrl1;
			    $banner->picture2 = $ImageUrl2;
			    $banner->save();
			    return redirect('/home/manageBanner')->with('message','Banner picture added successfully');	
	}

	public function manageBanner(){
		$banner = Banner::take(1)->first();
		return view('admin.manageBanner.manageBanner',['banner'=>$banner]);
	}

	public function editBanner($id){
		$banner = Banner::find($id);
		return view('admin.editBanner.editBanner',['banner'=>$banner]);
	}

	public function updateBanner(Request $request){
			$banner = Banner::find($request->id);
			if($request->hasFile('picture1')){
			//	unlink($banner->picture1);
					$picture1 = $request->file('picture1');
			    $Name = $picture1->getClientOriginalName();
			    $directory = 'banner-image/';
			    $ImageUrl1 = $directory.$Name;
			    $picture1->move($directory,$Name);
			    $banner->picture1 = $ImageUrl1;
			    $banner->save();
			     return redirect('home/manageBanner')->with('message1','Banner photo updated successfully');
			}
			elseif($request->hasFile('picture2')){
				//unlink($banner->picture2);
					$picture2 = $request->file('picture2');
			    $Name = $picture2->getClientOriginalName();
			    $directory = 'banner-image/';
			    $ImageUrl2 = $directory.$Name;
			    $picture2->move($directory,$Name);

			    $banner->picture2 = $ImageUrl2;
			    $banner->save();
			     return redirect('home/manageBanner')->with('message2','Banner photo updated successfully');
			}elseif($request->hasFile('picture1') and $request->hasFile('picture2')){

				$picture1 = $request->file('picture1');
			    $Name = $picture1->getClientOriginalName();
			    $directory = 'banner-image/';
			    $ImageUrl1 = $directory.$Name;
			    $picture1->move($directory,$Name);

			    $picture2 = $request->file('picture2');
			    $Name = $picture2->getClientOriginalName();
			    $directory = 'banner-image/';
			    $ImageUrl2 = $directory.$Name;
			    $picture2->move($directory,$Name);
			    
			    $banner->picture1 = $ImageUrl1;
			    $banner->picture2 = $ImageUrl2;
			    $banner->save();
			    return redirect('home/manageBanner')->with('message3','Banner photos updated successfully');
			}else{
				return redirect('home/manageBanner')->with('message4','You must be kidding , Before you have to select picturs'); 
			}
						    	
			   
	}

	public function deleteBanner($id){
		$banner = Banner::find($id);
		// unlink($banner->picture1);
		// unlink($banner->picture2);
		$banner->delete();
		return redirect('home/manageBanner')->with('message5','Banner photos deleted successfully');

	}

	public function addInfo(){
		$asset = Asset::all();
		return view('admin.addInfo.addInfo',['asset'=>$asset]);
	}

	public function saveInformation(Request $request){
		$this->validate($request,[
			'heading'=>'required',
			'number'=>'required',
			'publication_status'=>'required'
		]);

		$asset = new Asset();
		$asset->heading = $request->heading;
		$asset->number = $request->number;
		$asset->publication_status = $request->publication_status;
		$asset->save();
		return redirect('home/addInfo')->with('message','Info added successfully');
	}

	public function editInfo($id){
		$asset = Asset::find($id);
		return view('admin.editInfo.editInfo',['asset'=>$asset]);
	}

	public function updateInfo(Request $request){
			$asset = Asset::find($request->id);
			$asset->heading = $request->heading;
			$asset->number = $request->number;
			$asset->publication_status = $request->publication_status;
			$asset->save();
			return redirect('home/addInfo')->with('message2','Info edited successfully');
	}

	public function deleteInfo($id){
			$asset = Asset::find($id);
			$asset->delete();
			return redirect('home/addInfo')->with('message3','Info deleted successfully');
	}

	public function addEducation(){
		return view('admin.addEducation.addEducation');
	}

	public function saveEducation(Request $request){
		$this->validate($request,[
			'degree'=>'required|unique:education',
			'status'=>'required',
			'institute'=>'required',
			'year'=>'required',
			'gpa'=>'required',
			'result'=>'required',
			'of'=>'required',
		]);
		$education = new Education();
		$education->degree = $request->degree;
		$education->status = $request->status;
		$education->institute = $request->institute;
		$education->year = $request->year;
		$education->result =$request->result; 
		$education->gpa = $request->gpa;
		$education->of = $request->of;
		$education->save();
		return redirect('home/manageEducation')->with('message2','Education indo added successfully');
	}

	public function manageEducation(){
		$education = Education::all();
		return view('admin.manageEducation.manageEducation',['education'=>$education]);
	}

	public function deleteEducation($id){
		$education = Education::find($id);
		$education->delete();
		return redirect('home/manageEducation')->with('message','Education info deleted successfully');
	}

	public function addService(){
		return view('admin.addService.addService');
	}

	public function manageService(){
		$service = Service::all();
		return view('admin.manageService.manageService',['service'=>$service]);
	}

	public function saveService(Request $request){
		$this->validate($request,[
			'title'=>'required',
			'company'=>'required',
			'position'=>'required',
			'location'=>'required',
			'year'=>'required',
			'publication_status'=>'required',
		]);
		$service = new Service();
		$service->title = $request->title;
		$service->company = $request->company;
		$service->position = $request->position;
		$service->location = $request->location;
		$service->year = $request->year;
		$service->publication_status = $request->publication_status;
		$service->save();
		return redirect('home/manageService')->with('message','Service info added successfully');

	}

	public function deleteService($id){
		$service = Service::find($id);
		$service->delete();
		return redirect('home/manageService')->with('message2','Service info deleted successfully');
	}

	public function addReason(){
		return view('admin.addReason.addReason');
	}

	public function saveReason(Request $request){
		$this->validate($request,[
			'heading'=>'required',
			'description'=>'required'
		]);
		$reason = new Reason();
		$reason->heading = $request->heading;
		$reason->description = $request->description;
		$reason->save();
		return redirect('home/manageReason')->with('message','Reason info added successfully');
	}

	public function manageReason(){
		$reason = Reason::all();
		return view('admin.manageReason.manageReason',['reason'=>$reason]);
	}

	public function deleteReason($id){
		$reason = Reason::find($id);
		$reason->delete();
	return redirect('home/manageReason')->with('message2','Reason info deleted successfully');

	}

	public function addPhoto(){
		return view('admin.addPhoto.addPhoto');
	}

	public function savePhoto(Request $request){
	$this->validate($request,[
		'photo'=>'required',
		'publication_status'=>'required'
	]);
	$Image = $request->file('photo');
    $Name = $Image->getClientOriginalName();
    $directory = 'gallery/';
    $ImageUrl = $directory.$Name;
    $Image->move($directory,$Name);

    $photo = new Photo();
    $photo->photo = $ImageUrl;
    $photo->about = $request->about;
    $photo->publication_status = $request->publication_status;
	$photo->save();
	return redirect('home/managePhoto')->with('message','Photo added successfully');	
	}

	public function managePhoto(){
		$photo = Photo::all();
		return view('admin.managePhoto.managePhoto',['photo'=>$photo]);
	}

	public function deletePhoto($id){
		$photo = Photo::find($id);
		//unlink($photo->photo);
		$photo->delete();
		return redirect('home/managePhoto')->with('message2','Photo deleted successfully');
	}

	public function unpublishPhoto($id){
		$photo = Photo::find($id);
		$photo->publication_status = 0;
		$photo->save();
		return redirect('home/managePhoto')->with('message3','Photo unpublished successfully');
	}

	public function publishPhoto($id){
		$photo = Photo::find($id);
		$photo->publication_status = 1;
		$photo->save();
		return redirect('home/managePhoto')->with('message4','Photo published successfully');
	}

	public function addReference(){
		return view('admin.addReference.addReference');
	}

	public function manageReference(){
		$reference = Reference::all();
		return view('admin.manageReference.manageReference',['reference'=>$reference]);
	}

	public function saveReference(Request $request){
		$this->validate($request,[
			'type'=>'required',
			'name'=>'required',
			'profession'=>'required',
			'email'=>'required',
			'publication_status'=>'required'
		]);	
			if($request->hasFile('image')){
			$Image = $request->file('image');
		    $Name = $Image->getClientOriginalName();
		    $directory = 'familiers/';
		    $ImageUrl = $directory.$Name;
		    $Image->move($directory,$Name);
		   }

		$reference = new Reference();
		$reference->type = $request->type;
		$reference->name = $request->name;
		$reference->profession = $request->profession;
		$reference->email = $request->email;
		$reference->facebook = $request->facebook;
		$reference->twitter = $request->twitter;
		
		if($request->hasFile('image')){
		$reference->image = $ImageUrl;
		}
		else{
			$reference->image = $request->image;
		}
		$reference->publication_status = $request->publication_status;
		$reference->save();
		return redirect('home/manageReference')->with('message','Reference added successfully');
	}

	public function deleteReference($id){
		$reference = Reference::find($id);
		//unlink($reference->image);
		$reference->delete();
		return redirect('home/manageReference')->with('message2','Reference deleted successfully');
	}

	public function pubRef($id){
		$reference = Reference::find($id);
		$reference->publication_status = 1;	
		$reference->save();
		return redirect('home/manageReference')->with('message3','Reference published successfully');
	}

	public function unpubRef($id){
		$reference = Reference::find($id);
		$reference->publication_status = 0;	
		$reference->save();
		return redirect('home/manageReference')->with('message4','Reference unpublished successfully');
	}

	public function changePassword(Request $request){
		$this->validate($request,[
			'oldPassword'=>'required',
			'newPassword'=>'required'
		]);
		$user = User::find($request->id);
		if(password_verify($request->oldPassword, $user->password)){
			$user->password = bcrypt($request->newPassword);
			$user->save();
			return redirect('home')->with('sm','Password changed successfully');
		}else{
			return redirect('home')->with('sm2','Please enter valid password');
		}
	}

	public function SohanCV(Request $request){
		$this->validate($request,[
			'check'=>'required'
		]);

		$user = User::take(1)->first();

		if($user->name == $request->check){
			$data = Cv::find($request->id);
			return view('admin.showCv.showCv',['data'=>$data]);
		}else{
			return redirect('/');
		}
		

	}


	public function addCv(){
		return view('admin.addCv.addCv');
	}

	public function manageCv(){
		$cv = Cv::all();
		return view('admin.manageCv.manageCv',['cv'=>$cv]);
	}

	public function saveCv(Request $request){
		$this->validate($request,[
			'file'=>'required|mimes:pdf'
		]);
		$data = new Cv();

		$file = $request->file('file');
		$fileName = $file->getClientOriginalName();
		$request->file->move('storage/',$fileName);

		$data->file = $fileName;
		$data->publication_status = $request->publication_status;
		$data->about = $request->about;
		$data->save();
		return redirect('home/manageCv')->with('message','Cv added successfully');
	}

	public function cvView($id){
		$data = Cv::find($id);
		return view('admin.showCv.showCv',['data'=>$data]);
	}

	public function cvDownload($file){
		return response()->download('storage/'.$file);
	}

	public function deleteCv($id){
		$cv = Cv::find($id);
		$cv->delete();
		return redirect('home/manageCv')->with('message2','Cv deleted successfully');
	}

	public function unCv($id){
		$cv = Cv::find($id);
		$cv->publication_status = 0;
		$cv->save();
		return redirect('home/manageCv')->with('message3','Cv unpublished successfully');
	}

	public function pubCv($id){
		$cv = Cv::find($id);
		$cv->publication_status = 1;
		$cv->save();
		return redirect('home/manageCv')->with('message4','Cv published successfully');
	}
















}




