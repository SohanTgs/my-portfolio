<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Contact;
use App\Sent;

class SentEmail extends Controller
{
    public function inbox(){
    	$contact = Contact::all();
    	return view('admin.inbox.inbox',['contact'=>$contact]);
    }

	public function sentMail(){
		$sent = Sent::all();
    	return view('admin.sentMail.sentMail',['sent'=>$sent]);
    }

	public function compose(){
    	return view('admin.compose.compose');
    }

    public function contact(Request $request){
   	$this->validate($request,[
   		'name'=>'required',
   		'message'=>'required',
   		'email'=>'required',
   		'subject'=>'required'
   	]);
    	$data = array(
    			'name'=>$request->name,
    			'body'=>$request->message,
    			'email'=>$request->email,
    			'subject'=>$request->subject,
    				);
    		Mail::send('sohan.contact.contact',$data,function ($message) use ($data) {
    		$message->from('imsohan000@gmail.com',$data['name'])
    				->to('imsohan000@gmail.com','Sohan Tgs')
    				->subject($data['subject']);
                });

    	$contact = new Contact();
    	$contact->name = $request->name;
    	$contact->message = $request->message;
    	$contact->email = $request->email;
    	$contact->subject = $request->subject;
    	$contact->save();

    		return redirect('/')->with('message','We have recieved your email');
    		
    }

    public function deleteContact($id){
    	$contact = Contact::find($id);
    	$contact->delete();
    	return redirect('/home/inbox')->with('message','Inbox mail deleted succesfully');
    }


    public function sentEmail(Request $request){
    	$this->validate($request,[
    		'name'=>'required',
    		'message'=>'required',
    		'email'=>'required',
    		'subject'=>'required'
    	]);
    	$data = array(
    			'name'=>$request->name,
    			'body'=>$request->message,
    			'email'=>$request->email,
    			'subject'=>$request->subject,
    				);
    		Mail::send('sohan.sentEmail.sentEmail',$data,function ($message) use ($data) {
    		$message->from('imsohan000@gmail.com','Mohammad Sohan Tgs')
    				->to($data['email'],$data['name'])
    				->subject($data['subject']);
                });

    	$sent = new Sent();
    	$sent->name = $request->name;
    	$sent->message = $request->message;
    	$sent->email = $request->email;
    	$sent->subject = $request->subject;
    	$sent->save();

    		return redirect('/home/compose')->with('message','Sent e-mail succesfully');
    	}

    	public function deleteSentEmail($id){
    		$sent = Sent::find($id);
    		$sent->delete();
    		return redirect('/home/sentMail')->with('message','Deleted e-mail succesfully');
    	}











}




