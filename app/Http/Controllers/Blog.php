<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\MyBlog;

class Blog extends Controller
{
    public function addCategory(){
    	return view('admin.addCategory.addCategory');
    }

    public function saveCategory(Request $request){
    	$this->validate($request,[
    		'category'=>'required|unique:categories'
    	]);

    	$category = new Category();
    	$category->category = $request->category;
    	$category->description = $request->description;
    	$category->save();
    	return redirect('home/manageCategory')->with('message','Category added successfully');
    }

    public function manageCategory(){
    	$category = Category::all();
    	return view('admin.manageCategory.manageCategory',['category'=>$category]);
    }

    public function deleteCategory($id){
    	$category = Category::find($id);
    	$category->delete();
    	return redirect('home/manageCategory')->with('message2','Category deleted successfully');
    }

    public function addBlog(){
    	$category = Category::all();
    	return view('admin.addBlog.addBlog',['category'=>$category]);
    }

    public function saveBlog(Request $request){
    	$this->validate($request,[
    		'category_id'=>'required',
    		'name'=>'required',
    		'date'=>'required',
    		'short'=>'required',
    		'long'=>'required',
    		'image'=>'required',
    		'website'=>'required',
    		'publication_status'=>'required',
    	]);
    	$Image = $request->file('image');
		$Name = $Image->getClientOriginalName();
	    $directory = 'blog-image/';
		$ImageUrl1 = $directory.$Name;
		$Image->move($directory,$Name);

		$blog = new MyBlog();
		$blog->category_id = $request->category_id;
		$blog->name = $request->name;
		$blog->date = $request->date;
		$blog->short = $request->short;
		$blog->long = $request->long;
		$blog->extra = $request->extra;
		$blog->image = $ImageUrl1;
		$blog->website = $request->website;
		$blog->publication_status = $request->publication_status;
		$blog->save();
		return redirect('home/manageBlog')->with('message','Blog added successfully');

    }

    public function manageBlog(){
    	$blog = MyBlog::all();
    	return view('admin.manageBlog.manageBlog',['blog'=>$blog]);
    }

    public function deleteBlog($id){
    	$blog = MyBlog::find($id);
    	unlink($blog->image);
    	$blog->delete();
    	return redirect('home/manageBlog')->with('message2','Blog deleted successfully');
    }

    public function unPub($id){
    	$blog = MyBlog::find($id);
    	$blog->publication_status = 0;
    	$blog->save();
    	return redirect('home/manageBlog')->with('message3','Blog unpublished successfully');
    }

    public function Pub($id){
    	$blog = MyBlog::find($id);
    	$blog->publication_status = 1;
    	$blog->save();
    	return redirect('home/manageBlog')->with('message4','Blog published successfully');
    }





















}
